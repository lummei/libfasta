/* File: unittest.c
 * Description: A Unit Test program for the libfasta library API functions
 * Author: Daniel Garrigan Lummei Analytics, LLC
 * Date: January 2016
 * License: MIT
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <sys/stat.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "fasta.h"

/* Report sizes are 2kb */
#define REPORT_SIZE 2048

/* Function prototypes */
void print_intro (void);
void show_functions (void);
void show_report (void);
void load_file (char *filename);
void print_help (void);
void help_show (void);
void help_load (void);
void help_run (void);
void help_write (void);
void write_report (void);
int run_add_entry (void);
int run_avg_qual_bypos (void);
int run_convert_fastq_score (void);
int run_delete_entry (void);
int run_fetch_qual (void);
int run_fetch_seq (void);
int run_fetch_subqual (void);
int run_fetch_subseq (void);
int run_filter_fastq (void);
int run_filter_fastq_paired (void);
int run_get_num_seqs (void);
int run_get_fastq_score (void);
int run_get_seq_length (void);
int run_get_seq_names (void);
int run_is_fasta (void);
int run_is_fastq (void);
int run_is_paired (void);
int run_read_fasta (void);
int run_read_fastq (void);
int run_revcom (void);
int run_search_fasta (void);
int run_trim_fastq (void);
int run_trim_fastq_paired (void);
int run_write_fasta (void);
int run_write_fastq (void);

/* Globally scoped variables */
FADB *h;
char infile1[FILENAME_MAX];
char infile2[FILENAME_MAX];
char outfile[FILENAME_MAX];
char *report;
const int num_functions = 25;
const int num_commands = 5;

int (*func_array[25])(void) = { &run_add_entry, &run_avg_qual_bypos,
                                &run_convert_fastq_score, &run_delete_entry,
                                &run_fetch_qual, &run_fetch_seq,
                                &run_fetch_subqual, &run_fetch_subseq,
                                &run_filter_fastq, &run_filter_fastq_paired,
                                &run_get_num_seqs, &run_get_fastq_score, &run_get_seq_length,
                                &run_get_seq_names, &run_is_fasta, &run_is_fastq,
                                &run_is_paired, &run_read_fasta, &run_read_fastq,
                                &run_revcom, &run_search_fasta,
                                &run_trim_fastq, &run_trim_fastq_paired,
                                &run_write_fasta, &run_write_fastq
                              };

char **functions = (char *[])
{
    "add_entry", "avg_qual_bypos",
    "convert_fastq_score", "delete_entry", "fetch_qual", "fetch_seq",
    "fetch_subqual", "fetch_subseq", "filter_fastq", "filter_fastq_paired",
    "get_num_seqs", "get_fastq_score", "get_seq_length", "get_seq_names",
    "is_fasta", "is_fastq", "is_paired", "read_fasta", "read_fastq",
    "revcom", "search_fasta", "trim_fastq", "trim_fastq_paired", "write_fasta",
    "write_fastq"
};

void (*cmd_array[5])(void) = { &help_load, &help_show, &help_run, &help_write,
                               &print_help
                             };

char **commands = (char *[])
{"load", "show", "run", "write", "help"
};

int
main(int argc, char *argv[])
{
    int i = 0;
    char *line = NULL;
    char *tok = NULL;
    char sep[] = " ";

    /* Print welcome message */
    print_intro();

    /* Allocate memory for report */
    report = malloc (REPORT_SIZE * sizeof (char));

    /* Get commands from tester */
    while (1)
        {
            line = readline ("> ");
            if (line != NULL)
                {
                    add_history (line);
                    tok = strtok (line, sep);
                    if (tok != NULL)
                        {
                            /* show command */
                            if (strcmp (tok, "show") == 0)
                                {
                                    tok = strtok (NULL, sep);
                                    if (tok != NULL)
                                        {
                                            if (strcmp (tok, "functions") == 0)
                                                {
                                                    show_functions ();
                                                }
                                            else if (strcmp (tok, "report") == 0)
                                                {
                                                    show_report ();
                                                }
                                            else
                                                {
                                                    printf ("\"%s\" not a valid subcommand. "
                                                            "See \"help show\"\n", tok);
                                                }
                                        }
                                    else
                                        {
                                            printf ("Need subcommand for show. "
                                                    "See \"help show\"\n");
                                        }
                                }
                            /* run command */
                            else if (strcmp (tok, "run") == 0)
                                {
                                    tok = strtok (NULL, sep);
                                    if (tok != NULL)
                                        {
                                            for (i = 0; i < num_functions; i++)
                                                {
                                                    if (strcmp (functions[i], tok) == 0)
                                                        {
                                                            (*func_array[i])();
                                                            break;
                                                        }
                                                }
                                            if (i == num_functions)
                                                {
                                                    printf ("function \"%s\" not recognized\n", tok);
                                                }
                                        }
                                    else
                                        {
                                            printf ("Need subcommand for run. "
                                                    "See \"help run\"\n");
                                        }
                                }
                            /* load command */
                            else if (strcmp (tok, "load") == 0)
                                {
                                    tok = strtok (NULL, sep);
                                    if (tok != NULL)
                                        {
                                            load_file (tok);
                                        }
                                    else
                                        {
                                            printf ("Need filename for load. "
                                                    "See \"help load\"\n");
                                        }
                                }
                            /* write command */
                            else if (strcmp (tok, "write") == 0)
                                {
                                    tok = strtok (NULL, sep);
                                    if (tok != NULL)
                                        {
                                            if (strcmp (tok, "report") == 0)
                                                {
                                                    write_report ();
                                                }
                                            else
                                                {
                                                    printf ("subcommand \"%s\" not recognized\n", tok);
                                                }
                                        }
                                    else
                                        {
                                            printf ("Need subcommand for write. "
                                                    "See \"help write\"\n");
                                        }
                                }
                            /* help command */
                            else if (strcmp (tok, "help") == 0)
                                {
                                    tok = strtok (NULL, sep);
                                    if (tok != NULL)
                                        {
                                            for (i = 0; i < num_commands; i++)
                                                {
                                                    if (strcmp (commands[i], tok) == 0)
                                                        {
                                                            (*cmd_array[i])();
                                                            break;
                                                        }
                                                }
                                            if (i == num_commands)
                                                {
                                                    printf ("command \"%s\" not recognized\n", tok);
                                                }
                                        }
                                    else
                                        {
                                            print_help();
                                        }
                                }
                            else if ((strcmp (tok, "exit") == 0) ||
                                     (strcmp (tok, "quit") == 0))
                                {
                                    break;
                                }
                            else
                                {
                                    printf ("\"%s\" not a valid command. "
                                            "See \"help\".\n", tok);
                                }
                        }
                }
        }
    return 0;
}

void
print_help (void)
{
    printf ("Command     Argument     Description\n");
    printf ("-------     ----------   -----------\n");
    printf ("load        <filename>   Load data file into memory\n");
    printf ("show        functions    Display list of all libfasta functions\n");
    printf ("show        report       Display last report\n");
    printf ("write       report       Write last report to file\n");
    printf ("run         <function>   Run a libfasta library function\n");
    printf ("help        <command>    Display help on these commands\n");
    printf ("help        n/a          Show this message\n");
}

void
help_show (void)
{
    printf ("show command usage:\n");
    printf ("-------------------\n");
    printf ("\"show functions\": produce a list of all libfasta library functions\n");
    printf ("\"show report\": display the report from the last completed unit test\n");
}

void
help_load (void)
{
    printf ("load command usage:\n");
    printf ("-------------------\n");
    printf ("\"load <filename>\": loads a data file into memory\n");
    printf ("The string substituted into <filename> above needs to\n");
    printf ("be the full path to the data file relative to the current\n");
    printf ("working directory.\n");
}

void
help_run (void)
{
    printf ("run command usage:\n");
    printf ("------------------\n");
    printf ("\"run <function>\": executes the given libfasta library function\n");
    printf ("The string substituted into <function> above needs to\n");
    printf ("correspond to one of the function names given by the\n");
    printf ("\"show functions\" command\n");
}
void
help_write (void)
{
    printf ("write command usage:\n");
    printf ("--------------------\n");
    printf ("\"write report\": write the last unit test report to a text file\n");
    printf ("The default report output file name is \"unittest.txt\".\n");
    printf ("Subsequent calls to \"write report\" append to the end of the output file.\n");
}

void
print_intro (void)
{
    printf ("libfasta unit testing program\n");
    printf ("please enter a command or enter \"help\"\n");
    printf ("to see a list of commands\n");
}

void
show_functions (void)
{
    printf ("List of libfasta functions:\n");
    printf ("1. add_entry             12. get_fastq_score    23. trim_fastq_paired\n");
    printf ("2. avg_qual_bypos        13. get_seq_length     24. write_fasta\n");
    printf ("3. convert_fastq_score   14. get_seq_names      25. write_fastq\n");
    printf ("4. delete_entry          15. is_fasta\n");
    printf ("5. fetch_qual            16. is_fastq\n");
    printf ("6. fetch_seq             17. is_paired\n");
    printf ("7. fetch_subqual         18. read_fasta\n");
    printf ("8. fetch_subseq          19. read_fastq\n");
    printf ("9. filter_fastq          20. revcom\n");
    printf ("10. filter_fastq_paired  21. search_fasta\n");
    printf ("11. get_num_seqs         22. trim_fastq\n");
}

void
show_report (void)
{
    if (report[0] != '\0')
        {
            printf ("%s\n", report);
        }
    else
        {
            printf ("A report has not yet been generated.\n");
        }
}

void
load_file (char *tok)
{
    gzFile fq;

    if (tok != NULL)
        {
            strcpy (infile1, tok);
            if ((fq = gzopen (infile1, "rb")) == Z_NULL)
                {
                    fprintf (stderr, "Error: cannot open the input file %s\n", infile1);
                    return;
                }
            else
                {
                    printf ("%s is a valid file.\n", infile1);
                    printf ("Use the \"run read_fastx\" commands to read into memory\n");
                }
            gzclose (fq);
        }
    else
        {
            fprintf (stderr, "Need to supply filename to load command.\n");
            return;
        }
}

void
write_report (void)
{
    FILE *fp;

    if (strcmp (outfile, "unittest.txt") != 0)
        {
            memset (outfile, 0, FILENAME_MAX * sizeof (char));
            outfile[0] = '\0';
            strcpy (outfile, "unittest.txt");
        }

    if ((fp = fopen (outfile, "a")) == NULL)
        {
            fprintf (stderr, "Error opening output file.\n");
            return;
        }

    if (report[0] != '\0')
        {
            fprintf (fp, "%s\n", report);
        }
    else
        {
            printf ("A report has not yet been generated.\n");
        }
    fclose (fp);
}

int
run_add_entry (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int result = 0;
            char line[80];
            char identifier[] = "new_sequence";
            char sequence[] = "AAAAAAAAAAA";
            char *fetched = NULL;
            printf ("Running library function add_entry()\n");
            printf ("Adding key: %s\nwith sequence %s\n", identifier, sequence);
            result = add_entry (h, identifier, sequence);
            fetched = fetch_seq (h, identifier);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for add_entry function\n");
            strcat (report, "***************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (line, "%d\nResult of fetch_seq: %s\n", result, fetched);
            strcat (report, line);
            printf ("%s", report);
            return 0;
        }
}

int
run_avg_qual_bypos (void)
{
    printf ("Running library function avg_qual_bypos()\n");
    return 0;
}

int
run_convert_fastq_score (void)
{
    printf ("Running library function convert_fastq_score()\n");
    return 0;
}

int
run_delete_entry (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs1 = 0;
            int nseqs2 = 0;
            int ran1 = 0;
            int result = 0;
            char **names = NULL;
            char buffer[200];
            char *result_fetch = NULL;
            nseqs1 = hash_size(h);
            printf ("Running library function delete_entry()\n");
            printf ("Test will delete an entry from the database.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs1;
            result = delete_entry (h, names[ran1]);
            nseqs2 = hash_size(h);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for delete_entry function\n");
            strcat (report, "******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%d\nEntry %s will be deleted\nNumber of sequences "
                     "before: %d\nNumber of sequences after: %d\n", result,
                     names[ran1], nseqs1, nseqs2);
            strcat (report, buffer);
            memset (buffer, 0, 200 * sizeof (char));
            printf ("Running fetch_seq on deleted entry...\n");
            result_fetch = fetch_seq (h, names[ran1]);
            sprintf (buffer, "fetch_seq returned: %p\n", result_fetch);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs1; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result_fetch);
            return 0;
        }
}

int
run_fetch_qual (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            char **names = NULL;
            char buffer[200];
            char *result = NULL;
            nseqs = hash_size(h);
            printf ("Running library function fetch_qual()\n");
            printf ("Test will print the results from fetching the quality "
                    "string of a random sequence.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs;
            result = fetch_qual (h, names[ran1]);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for fetch_qual function\n");
            strcat (report, "****************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%p\nSequence name: %s\nQuality string: %s\n",
                     result,  names[ran1], result);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result);
            return 0;
        }
}

int
run_fetch_seq (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            char **names = NULL;
            char buffer[200];
            char *result = NULL;
            nseqs = hash_size(h);
            printf ("Running library function fetch_seq()\n");
            printf ("Test will print the results from fetching a random sequence.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs;
            result = fetch_seq (h, names[ran1]);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for fetch_seq function\n");
            strcat (report, "***************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%p\nSequence name: %s\nSequence string: %s\n",
                     result,  names[ran1], result);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result);
            return 0;
        }
}

int
run_fetch_subqual (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            char **names = NULL;
            char buffer[200];
            char *result = NULL;
            nseqs = hash_size(h);
            printf ("Running library function fetch_subqual()\n");
            printf ("Test will print the results from fetching the quality "
                    "substring of a random sequence.\n");
            printf ("Positions 10 through 30 will be fetched.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs;
            result = fetch_subqual (h, names[ran1], 10, 30);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for fetch_subqual function\n");
            strcat (report, "*******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%p\nSequence name: %s\nQuality string: %s\n",
                     result,  names[ran1], result);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result);
            return 0;
        }
}

int
run_fetch_subseq (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            char **names = NULL;
            char buffer[200];
            char *result = NULL;
            nseqs = hash_size(h);
            printf ("Running library function fetch_subseq()\n");
            printf ("Test will print the results from fetching a random subsequence.\n");
            printf ("Positions 10 through 30 will be fetched.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs;
            result = fetch_subseq (h, names[ran1], 10, 30);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for fetch_subseq function\n");
            strcat (report, "******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%p\nSequence name: %s\nSequence string: %s\n",
                     result,  names[ran1], result);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result);
            return 0;
        }
}

int
run_filter_fastq (void)
{
    printf ("Running library function filter_fastq()\n");
    return 0;
}

int
run_filter_fastq_paired (void)
{
    printf ("Running library function filter_fastq_paired()\n");
    return 0;
}

int
run_get_num_seqs (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int ns = 0;
            char line[80];
            printf ("Running library function get_num_seqs()\n");
            printf ("Reading database...\n");
            ns = get_num_seqs (h);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for get_num_seqs function\n");
            strcat (report, "******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (line, "%d\n", ns);
            strcat (report, line);
            printf ("%s", report);
            return 0;
        }
}

int
run_get_fastq_score (void)
{
    printf ("Running library function get_fastq_score()\n");
    return 0;
}

int
run_get_seq_length (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            int ran2 = 0;
            size_t length1 = 0;
            size_t length2 = 0;
            char buffer[200];
            char **names = NULL;
            nseqs = hash_size(h);
            names = get_seq_names (h);
            printf ("Running library function get_seq_length()\n");
            printf ("Test will print the lengths of two random sequences.\n");
            printf ("Reading database...\n");
            ran1 = rand() % nseqs;
            do
            {
                ran2 = rand() % nseqs;
            } while (ran1 == ran2);
            length1 = get_seq_length (h, names[ran1]);
            length2 = get_seq_length (h, names[ran2]);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for get_seq_length function\n");
            strcat (report, "********************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value 1: ");
            sprintf (buffer, "%lu\nFunction returned value 2: %lu\nName of first "
                             "seq: %s\nName of second seq: %s\n", length1, length2,
                             names[ran1], names[ran2]);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            return 0;
        }
}

int
run_get_seq_names (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            char **names = NULL;
            char buffer[200];
            nseqs = hash_size(h);
            printf ("Running library function get_seq_names()\n");
            printf ("Test will print the names of the first and last sequence names.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for get_seq_names function\n");
            strcat (report, "******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%p\nFirst name: %s\nLast name: %s\nLast seq name "
                     "is number %d.\n", names,  names[0], names[nseqs - 1], nseqs);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            return 0;
        }
}

int
run_is_fasta (void)
{
    printf ("Running library function is_fasta()\n");
    return 0;
}

int
run_is_fastq (void)
{
    printf ("Running library function is_fastq()\n");
    return 0;
}

int
run_is_paired (void)
{
    printf ("Running library function is_paired()\n");
    return 0;
}

int
run_read_fasta (void)
{
    char line[80];

    printf ("Running library function read_fasta()\n");
    h = NULL;
    printf ("Reading data from file %s...\n", infile1);
    h = read_fasta (infile1);
    printf ("Creating blank report...\n");
    memset (report, 0, REPORT_SIZE * sizeof (char));
    report[0] = '\0';
    strcat (report, "Unit test report for read_fasta function\n");
    strcat (report, "****************************************\n");
    strcat (report, "Input file: ");
    strcat (report, infile1);
    strcat (report, "\nFunction returned value: ");
    sprintf (line, "%p\n", h);
    strcat (report, line);
    printf ("%s", report);

    return 0;
}

int
run_read_fastq (void)
{
    char line[80];

    printf ("Running library function read_fastq()\n");
    h = NULL;
    printf ("Reading data from file %s...\n", infile1);
    h = read_fastq (1, infile1);
    printf ("Creating blank report...\n");
    memset (report, 0, REPORT_SIZE * sizeof (char));
    report[0] = '\0';
    strcat (report, "Unit test report for read_fastq function\n");
    strcat (report, "****************************************\n");
    strcat (report, "Input file: ");
    strcat (report, infile1);
    strcat (report, "\nFunction returned value: ");
    sprintf (line, "%p\n", h);
    strcat (report, line);
    printf ("%s", report);

    return 0;
}

int
run_revcom (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            char **names = NULL;
            char buffer[200];
            char *result1 = NULL;
            char *result2 = NULL;

            nseqs = hash_size(h);
            printf ("Running library function revcom()\n");
            printf ("Test will print the results from reverse complementing "
                    " a random subsequence.\n");
            printf ("Positions 10 through 30 will be fetched.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs;
            result1 = fetch_subseq (h, names[ran1], 10, 30);
            result2 = revcom (result1);
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for fetch_subseq function\n");
            strcat (report, "******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%p\nSequence name: %s\nOriginal sequence string: "
                             "%s\nREVCOM string: %s\n", result2,  names[ran1],
                             result1, result2);
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result1);
            free (result2);
            return 0;
        }
}

int
run_search_fasta (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int i = 0;
            int nseqs = 0;
            int ran1 = 0;
            unsigned int pos = 0;
            char **names = NULL;
            char buffer[200];
            char *result1 = NULL;
            char *result2 = NULL;
            TREE *tree;

            nseqs = hash_size(h);
            printf ("Running library function search_fasta()\n");
            printf ("Test will print the results from search with a random subsequence.\n");
            printf ("Positions 10 through 30 will be fetched and then queried.\n");
            printf ("Reading database...\n");
            names = get_seq_names (h);
            ran1 = rand() % nseqs;
            result1 = fetch_seq (h, names[ran1]);
            printf ("Read target sequence from file, creating suffix tree\n");
            tree = create_tree (result1, strlen (result1));
            printf ("Fetching subsequence as query\n");
            result2 = fetch_subseq (h, names[ran1], 10, 30);
            pos = search_tree (tree, result2, strlen (result2));
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for fetch_subseq function\n");
            strcat (report, "******************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nTrue position of query: 10-30\n");
            sprintf (report, "Resulting position of query: %u - %u\n", pos,
                             pos + strlen (result2));
            strcat (report, buffer);
            printf ("%s", report);
            for (i = 0; i < nseqs; i++)
                {
                    free (names[i]);
                }
            free (names);
            free (result1);
            free (result2);
            return 0;
        }
}

int
run_trim_fastq (void)
{
    printf ("Running library function trim_fastq()\n");
    return 0;
}

int
run_trim_fastq_paired (void)
{
    printf ("Running library function trim_fastq_paired()\n");
    return 0;
}

int
run_write_fasta (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int ret = 0;
            size_t orig_size = 0;
            size_t new_size = 0;
            struct stat st1;
            struct stat st2;
            char buffer[200];
            printf ("Running library function write_fasta()\n");
            printf ("Test will write database to file and check resulting file size.\n");
            printf ("Writing database to file \"outtest.fa.gz\"...\n");
            ret = write_fasta (h, "outtest.fa.gz");
            stat (infile1, &st1);
            stat ("outtest.fa.gz", &st2);
            orig_size = st1.st_size;
            new_size = st2.st_size;
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for write_fasta function\n");
            strcat (report, "*****************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%d\nOriginal file size: %lu bytes\nNew file size: %lu bytes.\n",
                     ret,  orig_size, new_size);
            strcat (report, buffer);
            printf ("%s", report);
            return 0;
        }
}

int
run_write_fastq (void)
{
    if (h == NULL)
        {
            printf ("Need to load database using read_fastx functions\n");
            return 1;
        }
    else
        {
            int ret = 0;
            size_t orig_size = 0;
            size_t new_size = 0;
            struct stat st1;
            struct stat st2;
            char buffer[200];
            printf ("Running library function write_fastq()\n");
            printf ("Test will write database to file and check resulting file size.\n");
            printf ("Writing database to file \"outtest.fq.gz\"...\n");
            ret = write_fastq (h, "outtest.fq.gz");
            stat (infile1, &st1);
            stat ("outtest.fq.gz", &st2);
            orig_size = st1.st_size;
            new_size = st2.st_size;
            printf ("Creating blank report...\n");
            memset (report, 0, REPORT_SIZE * sizeof (char));
            report[0] = '\0';
            strcat (report, "Unit test report for write_fastq function\n");
            strcat (report, "*****************************************\n");
            strcat (report, "Input file: ");
            strcat (report, infile1);
            strcat (report, "\nFunction returned value: ");
            sprintf (buffer, "%d\nOriginal file size: %lu bytes\nNew file size: %lu bytes.\n",
                     ret,  orig_size, new_size);
            strcat (report, buffer);
            printf ("%s", report);
            return 0;
        }
}
