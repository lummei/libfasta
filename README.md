=======
# libfasta
****

## Description <a name="Description"></a>

`libfasta` is a C library that provides the foundation for building efficient 
low-level bioinformatics software. The library is currently version 0.8. The
goal of the project is to provide a repository for a wide range of commonly
used DNA sequence string manipulation algorithms, such that downstream 
development is made faster and easier. Although it is an ongoing, part-time
project for one person, the bioinformatics community is encouraged to
contribute, test, and benchmark their own code.

`libfasta` aims to provide the following functionality:
* Highly optimized I/O of either compressed or uncompressed fastA-formatted 
  sequence databases
* I/O of compressed or uncompressed fastQ-formatted sequences
* Compression of IUPAC alphabet in memory
* Parse Illumina fastQ identifier lines
* Fast lookup of entries in fast[A|Q] database
* Add/remove entries from fast[A|Q] database
* Extract subsequences from fast[A|Q] database entries
* Pull CDS from a fastA file based on information in a GFF file
* Trim or filter fastQ entries based on quality
* Identify and/or convert fastQ qualilty score scale
* Realign mate pairs in fastQ files
* Create and query suffix arrays for exact string matching
* Smith-Waterman pairwise alignment

****

## Table of Contents

1. [Installation](#Installation)
    * [Compiling the code](#Compiling)
    * [Test program](#Test)
    * [Uninstalling](#Uninstalling)
2. [Using the library](#Using)
    * [Example program](#Example)
    * [Including the header](#Including)
    * [Linking the library](#Linking)
    * [Data structures](#Data_structures)
    * [Global variables](#Global)
    * [List of functions](#List_functions)
        * [File I/O](#FileIO)
        * [Adding/Deleting database entries](#AddDel_entries)
        * [Database properties](#Database_properties)
        * [Sequence entries](#Sequence_entries)
        * [Quality scores](#Quality_scores)
        * [Filtering/Cleaning entries](#FiltClean_entries)
        * [Database search functions](#Search)
3. [Contributing code](#Contributing)
    * [Style](#Style)
    * [Testing](#Testing)
    * [Benchmarking](#Benchmarking)
4. [Additional information](#Additional_information)

****

## Installation <a name="Installation"></a>

### Compiling the code <a name="Compiling"></a>

Compilation of `libfasta` depends on the presence of the `zlib` run-time 
library and headers (e.g., `zlib.h`). The project uses `autotools` to build
the library. The first step is to type
```
./autogen.sh
```
Then, run the newly generated configure script,
```
./configure --prefix=/path/to/install
```
This will produce the needed Makefiles. The `--prefix` specification is
optional, allowing the user to install the components of the library to a custom
location. Otherwise, the default locations are `/usr/local/lib` for the 
library files and `/usr/local/include` for the `fasta.h` header file.
Then to compile both the static and run-time libraries, type
```
make
```
Next, to install the files into the default, or specified, location, just type
```
make install
```
Note: the installation of the library files into the non-home folders in the 
filesystem usually will require root privileges.

### Test program <a name="Test"></a>
The library includes C code for a unit test program, `unittest`. This code also
acts as an example for how to use the library functionality. The `unittest`
program depends upon the GNU `readline` library. To compile the `unittest`
program, simply type
```
make check
```
The compiled program is located in the `test/` directory.

### Uninstalling <a name="Uninstalling"></a>

Lastly, all compiled files can be removed from disk with the command
```
make clean
```
followed by
```
make distclean
````

****

## Using the library <a name="Using"></a>

### Example program <a name="Example"></a>

Below is a simple program using `libfasta` to read in a fastA formatted 
file, print out some information about the first sequence in the file,
and perform an exact string match.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fasta.h>

int main (int argc, char *argv[])
{
    int nseqs = 0;
    unsigned int i = 0;
    unsigned int seqlen = 0;
    unsigned int num_hits = 0;
    unsigned int first = 0;
    char **seqnames = NULL;
    char *seq = NULL;
    char *query = NULL;
    COMPRESS_SEQ *cs = NULL;
    FADB *h = NULL;
    SUFFIX_ARRAY *sa = NULL;

    /* Check if input file name is given as first argument */
    if (argc < 2)
        {
            fputs ("Need filename as input.\n", stderr);
            return 1;
        }

    /* Read in the fasta input file */
    if ((h = read_fasta (argv[1])) == NULL)
        {
            fputs ("Problem reading the input fasta file\n", stderr);
            return 1;
        }

    /* Get the number of sequences in the input fasta file */
    nseqs = get_num_seqs (h);

    /* Get the sequence identifiers from the input fasta file */
    seqnames = get_seq_names (h);

    /* Retrieve the sequence of the first entry in the fasta input file */
    seq = fetch_seq (h, seqnames[0]);

    /* Get the length of the first sequence */
    seqlen = get_seq_length (h, seqnames[0]);

    /* Compress the retrieved sequence */
    cs = compress_seq (seq);

    /* Create a suffix array from the first fasta sequence */
    if ((sa = suffix_array_create (seq)) == NULL)
        {
            fputs ("Error creating suffix array\n", stderr);
            return 1;
        }

    /* Construct a sample query sequence */
    query = malloc (5 * sizeof (char));
    strcpy (query, "GAAT\0");

    /* Search the suffix array for occurrences of the query string */
    num_hits = suffix_array_search (sa, query, &first);

    /* Print the positions of all occurrences of the query string */
    for (i = first; i < first + num_hits; i++)
        {
            fprintf (stdout, "%u\n", seqlen - (unsigned int)(strlen (sa->suffix[i])) + 1u);
        }

    /* Deallocate memory for suffix array */
    suffix_array_destroy (sa);

    free (query);

    return 0;
}
```

### Including the header <a name="Including"></a>

Using the `libfasta` library requires including the `fasta.h` header file
in your program, e.g.,
```
#include <fasta.h>
```
All of the library functions are defined in this single header file. By default,
the `fasta.h` header is installed to `/usr/local/include`. If your header file
is installed to a custom location, you may need to specify the `-I` switch 
when compiling your program.

### Linking the library <a name="Linking"></a>

Be sure to compile your executable by linking to the run-time library, e.g.,
```
gcc -o myprog myprog.c -lfasta
```
If the run-time libraries are in a path expected by `gcc`. In addition,
after the libraries are installed, an account with sudo privileges may
have to run `ldconfig`.

By default, both the static and run-time library files are installed to 
`/usr/local/lib`. If you installed the library files to a custom location, 
you may need to compile using the `-L` switch and specifying the 
`LD_LIBRARY_PATH` environment variable for execution of the program (if
you are using the run-time libraries).

### Data structures <a name="Data_structures"></a>

FADB: A hash table data structure to hold the sequence database.
```
typedef struct hash_table
{
    unsigned int n_buckets;
    unsigned int size;
    unsigned int n_occupied;
    unsigned int upper_bound;
    unsigned int scores;
    unsigned int *flags;
    char **keys;
    char **vals;
} FADB;
```

SUFFIX_ARRAY: Data structure to hold suffix array
```
typedef struct _suffix_array
{
    unsigned int n;        /* length of string including final null character */
    const char *string;    /* original string */
    const char **suffix;   /* suffix array of length n */
} SUFFIX_ARRAY;
```

COMPRESS_SEQ: A data structure to hold 4-bit compressed IUPAC alphabet
```
typedef struct _compressed_seq
{
    char *seq;
    unsigned int ulength;
    unsigned int clength;
} COMPRESS_SEQ;
```

parse_string: A data structure to hold parsed sequence coordinates
```
struct parse_string
{
  char *chr;                /* chromosome or contig identifier */
  unsigned long int start;  /* base start position on chromosome */
  unsigned long int end;    /* base end position on chromosome */
};
```

illumina_id_string: A data structure to hold Illumina identifier information
```
struct illumina_id_string
{
    char *instrument;       /*! Instrument ID */
    int run_number;         /*! Run number on instrument */
    char *flowcell_ID;      /*! Flowcell identifier string */
    int lane;               /*! Lane number */
    int tile;               /*! Tile number */
    int x_pos;              /*! X coordinate of cluster */
    int y_pos;              /*! Y coordinate of cluster */
    int read;               /*! Read number. 1 can be single read or read 2 of paired-end */
    int is_filtered;        /*! Non-zero if the read is filtered, zero otherwise */
    int control_number;     /*! 0 when none of the control bits are on, otherwise it is an even number */
    char *index sequence;   /*! Index sequence */
}
```

### Global variables <a name="Global"></a>

RSEP: Record separator character (corresponding to ASCII decimal 30). Used to
separate sequence and quality strings in fastQ database entries.
```
#define RSEP 0x001E
```

iupac_ext: The extended IUPAC alphabet with all 16 characters
```
char iupac_ext[17] = "ACGTRYSWKMBDHVN-\0";
```

### List of functions <a name="List_functions"></a>

#### File I/O <a name="FileIO"></a>

```
FADB *read_fasta (char *filename)
```
Read in fastA database from file.

Parameters:  
`filename` Pointer to fastA input filename string

Returns:  
Pointer to hash table on success, `NULL` on failure

```
void *read_fastq (int num_files,  ...)
```
Read in a fastQ database from one or two input files.

Parameters:  
`num_files` The number of fastQ input files to read from disk  
`...` list of pointers to input fastQ file names

Returns:  
Generic pointer to hash table on success, `NULL` on failure

```
int is_fasta (char *filename)
```
Determine if file is in fastA format.

Parameters:  
`filename` Pointer to fastA input filename string

Returns:  
Zero if file is not fastA and one if file is fastA

```
int is_fastq (char *filename)
```
Determine if file is in fastQ format.

Parameters:  
`filename` Pointer to fastQ input filename string

Returns:  
Zero if file is not fastQ and one if file is fastQ

```
int write_fasta (FADB *h, char *filename)
```
Writes a fastA database to file

Parameters:  
`h` Pointer to fastA database  
`filename` Pointer to fastA output filename string

Returns:  
Zero on success and non-zero on failure

```
int write_fastq (FADB *h, char *filename)
```
Writes a fastQ database to file

Parameters:  
`h` Pointer to fastQ database  
`filename` Pointer to fastQ output filename string

Returns:  
Zero on success and non-zero on failure

```
int write_fastq_paired (FADB *h, char *filename_base)
```
Writes a paired-end fastQ database to two output files

Parameters:  
`h` Pointer to fastQ database  
`filename_base` Pointer to base fastQ output filename string

Returns:  
Zero on success and non-zero on failure

```
int parse_region_string (char *string, struct parse_string *ps)
```
Parses a sequence region in the form `<chr>:<start>-<end>`. If
`:<start>-<end>` are omitted, the function assigns `start = 1` and `end =
ULONG_MAX`, it is up to the programmer to use this information to check
if a valid start and end position have been specified.

Parameters:  
`string` Pointer to the region string to be parsed  
`ps` Pointer to the data structure to hold to coordinates

Returns:  
Zero on success and non-zero on failure

```
int parse_illumina_id (char *line, struct illumina_id_string *iids)
```
Parse illumina-formatted identifier line from fastQ entry

Parameters:  
`string` Pointer to the fastQ identifier string to be parsed  
`iids` Pointer to the data structure to hold to illumina info

Returns:  
Zero on success and non-zero on failure

```
FADB *create_adaptor_database (char *filename)
```
Construct a database of adaptor sequences from input text file


Parameters:
`filename` Pointer to input file containing adaptor sequences

Returns:
Pointer to adaptor database on success and NULL on failure

#### Adding/Deleting database entries <a name="AddDel_entries"></a>

```
int add_entry (FADB *h, char *identifier, char *sequence)
```
Adds a new entry to a fast[A|Q] database.
  
Parameters:  
`h` Pointer to the fast[A|Q] database  
`identifier` Pointer to identifier of the new entry  
`sequence` Pointer to sequence of the new entry  
  
Returns:  
Zero on success and non-zero on failure  

```
int delete_entry (FADB *h, char *identifier)
```
Remove an existing entry from the fast[A|Q] database.

Parameters:  
`h` Pointer to the fast[A|Q] database  
`identifier` Pointer to identifier of the entry to be deleted

Returns:  
Zero on success and non-zero on failure  

#### Database properties <a name="Database_properties"></a>

```
int get_num_seqs (FADB *h)
```
Get the number of entries in fast[A|Q] database.

Parameters:  
`h` Pointer to the fast[A|Q] database

Returns:  
Integer of number of entries present in database

```
char **get_seq_names (FADB *h)
```
Get an array of entry identifiers.

Parameters:  
`h` Pointer to the fast[A|Q] database

Returns:  
Pointer to array of sequence identifier strings

```
int is_paired (FADB *h)
```
Determine if database comprises paired reads.

Parameters:  
`h` Pointer to fastQ database

Returns:  
Zero if database is not paired and one if database is paired

#### Sequence entries <a name="Sequence_entries"></a>

```
char *fetch_seq (FADB *h, char *query_string)
```
Fetchs the sequence of a fast[A|Q] database entry.

Parameters:  
`h` Pointer to fast[A|Q] database  
`query_string` Pointer to query identifier string

Returns:  
Pointer to sequence string

```
char *fetch_subseq (FADB *h, char *identifier, unsigned long int start, unsigned long int end)
```
Fetchs a substring from a sequence in a fast[A|Q] database entry.

Parameters:  
`h` Pointer to fast[A|Q] database  
`identifier` Pointer to query identifier string  
`start` First position in substring  
`end` End position in substring

Returns:  
Pointer to sequence string

```
size_t get_seq_length (FADB *h, char *identifier)
```
Get the length of a sequence entry.

Parameters:  
`h` Pointer to the fast[A|Q] database  
`identifier` A pointer to the query identifier string

Returns:  
Length of sequence on success and zero on failure

```
char *revcom (char *input_string)
```
Provides reverse complement of DNA string.

Parameters:  
`input_string` Pointer to string to reverse complement

Returns:  
Pointer to new memory address with reverse complement string

```
COMPRESS_SEQ *compress_seq (char *s)
```
Compresses IUPAC alphabet into 4-bit encoding

Parameters:  
`s` Pointer to IUPAC string to compress

Returns:  
Pointer to new memory address with 4-bit encoded string

```
char *uncompress_seq (COMPRESS_SEQ *cs)
```
Uncompresses 4-bit encoding of IUPAC alphabet

Parameters:  
`cs` Pointer to string to uncompress

Returns:  
Pointer to new memory address with uncompressed IUPAC string

#### Quality scores <a name="Quality_scores"></a>

```  
char *fetch_qual (FADB *h, char *query_string)
```
Fetchs the quality string of a fast[A|Q] database entry.

Parameters:  
`h` Pointer to fast[A|Q] database  
`query_string` Pointer to query identifier string
  
Returns:  
Pointer to sequence string

```
char *fetch_subqual (FADB *h, char *identifier, unsigned long int start, unsigned long int end)
```
Fetchs a substring from a quality string in a fast[A|Q] database entry.

Parameters:  
`h` Pointer to fast[A|Q] database  
`identifier` Pointer to query identifier string  
`start` First position in substring  
`end` End position in substring

Returns:  
Pointer to sequence string

```
int get_fastq_score (FADB *h)
```
Determine the scale of quality scores in a fastQ database.

Parameters:  
`h` Pointer to fastQ database

Returns:  
Integer that maps to the scale of the quality scores

```
double *avg_qual_bypos (FADB *h)
```
Calculates average quality by position for all entries in fastQ database.

Parameters:  
`h` Pointer to fastQ database

Returns:  
Pointer to array of floating point qualities

```
int convert_fastq_score (FADB *h, int orig_score, int new_score)
```
Convert the scale of quality scores in a fastQ database.

Parameters:  
`h` Pointer to fastQ database  
`orig_score` The original scale of quality scores in the fastQ database  
`new_score` The desired scale of quality scores in the fastQ database
  
Returns:  
Zero on success and non-zero on failure

#### Filtering/Cleaning entries <a name="FiltClean_entries"></a>

```
int filter_fastq (FADB *h, double propN)
```
Filter reads in a fastQ database based on the proportion of ambiguous characters.

Parameters:  
`h` Pointer to fastQ database  
`propN` Allowable proportion of ambiguous base calls

Returns:  
Zero on success and non-zero on failure
  
```
int filter_fastq_paired (FADB *h, double propN)
```
Filter paired reads in a fastQ database based on the proportion of ambiguous characters.

Parameters:  
`h` Pointer to fastQ database  
`propN` Allowable proportion of ambiguous base calls

Returns:  
Zero on success and non-zero on failure

```
int trim_fastq (FADB *h, int trim_quality, int min_length)
```
Trim the end of reads in a fastQ database.

Parameters:  
`h` Pointer to fastQ database  
`trim_quality` Trim quality threshold parameter  
`min_length` Minimum length of reads to keep after trimming

Returns:  
Zero on success and non-zero on failure

```
int trim_fastq_paired (FADB *h, int trim_quality, int min_length)
```
Trim the end of paired reads in a fastQ database.

Parameters:  
`h` Pointer to fastQ database  
`trim_quality` Trim quality threshold parameter  
`min_length` Minimum length of reads to keep after trimming

Returns:  
Zero on success and non-zero on failure

```
int trim_adaptor_seq (FADB *h, FADB *adb, int edit_dist)
```
Trim adaptor sequences from either 3' or 5' end of database entry

Parameters:
`h` Pointer to fastQ database
`adb` Pointer to the adaptor sequence database
`edit_dist` The maximum edit distance to be considered a match

Returns:
Zero on success and non-zero on failure

#### Database search functions <a name="Search"></a>

```
SUFFIX_ARRAY *suffix_array_create (const char *string)
```
Construct the suffix array

Parameters:  
`string` Pointer to string constituting the suffix array

Returns:  
Pointer to the suffix array data structure on success or `NULL` on failure

```
void suffix_array_destroy (SUFFIX_ARRAY *sa)
```
Deallocate memory for suffix array

Parameters:  
`sa` Pointer to suffix array data structre

```
unsigned int suffix_array_search (SUFFIX_ARRAY *sa, const char *substring, unsigned int *first)
```
Search the suffix array for the number of occurrences of a substring


Parameters:  
`sa` Pointer to the suffix array data structure  
`substring` Pointer to the query string  
`first` Pointer to positions of each occurrence

Returns:  
Number of occurrences of query string

```
char *suffix_array_BWT (SUFFIX_ARRAY *sa)
```
Get Burrows-Wheeler transform of underlying string

Parameters:  
`sa` Pointer to the suffix array data structure

Returns:  
Pointer to new memory holding Burrows-Wheeler transform of suffix array

```
char *inverse_BWT (unsigned int len, const char *s)
```
Invert BWT of null-terminated string

Parameters:  
`len`
`s`

Returns:  
Pointer to new memory containing original string

```
int levenshtein (char *s1, char *s2)
```
Calculates the Levenshtein distance between two strings

Parameters:
`s1` The first input string
`s2` The second input string

Returns: 
An integer quantifying the edit distance between the two strings

****

## Contributing code <a name="Contributing"></a>

### Style <a name="Style"></a>

To format your code for consistent style, please use the `astyle` program,
like so,
```
astyle --style=gnu <input.c>
```
to format your code in the GNU style.

### Testing <a name="Testing"></a>

It is also useful to update the `unittest` program to provide a test of
your new code.

### Benchmarking <a name="Benchmarking"></a>

More here soon.

****

## Additional information <a name="Additional_information"></a>

The libfasta library is provided under the conditions of the MIT license.
Users are encouraged to contribute or make suggestions. Please contact 
[dgarriga@lummei.net](mailto:dgarriga@lummei.net) if you have any further 
questions regarding `libfasta`.
>>>>>>> 9256584a29009da988b426088b6d66bbb7cb478b
