/*! \file fasta.h
 *  \brief Header file for the libfasta C library
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date August 2016
 *  \copyright MIT license
*/

#ifndef FASTA_H
#define FASTA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdarg.h>
#include <emmintrin.h>
#include <zlib.h>

#ifdef __GNUC__
#define LIKELY(x) __builtin_expect((x),1)
#define UNLIKELY(x) __builtin_expect((x),0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif

/*******************************************************************************
 * Data structure definitions
 ******************************************************************************/

typedef struct _fastq
{
    char *seq;
    char *qual;
    char *indiv_id;
    char *instrument;       /*! Instrument ID */
    int run_number;         /*! Run number on instrument */
    char *flowcell_ID;      /*! Flowcell identifier string */
    int lane;               /*! Lane number */
    int tile;               /*! Tile number */
    int x_pos;              /*! X coordinate of cluster */
    int y_pos;              /*! Y coordinate of cluster */
    int read;               /*! Read number. 1 can be single read or read 2 of paired-end */
    int is_filtered;        /*! Non-zero if the read is filtered, zero otherwise */
    int control_number;     /*! 0 when none of the control bits are on, otherwise it is an even number */
    int has_mate;           /*! booelan indicating whether read has been successfully paired */
    char *index_sequence;   /*! Index sequence */
    unsigned int mate;      /*! Current hash index of mated pair */
} FASTQ;

/*! \struct STDDB
 *  \brief Generic character hash table data structure
 */

typedef struct generic_hash_table
{
    unsigned int n_buckets;
    unsigned int size;
    unsigned int n_occupied;
    unsigned int upper_bound;
    unsigned int scores;
    unsigned int *flags;
    char **keys;
    char **vals;
} STDDB;

/*! \struct FQDB
 *  \brief fastQ hash table data structure
 */

typedef struct fastq_hash_table
{
    unsigned int n_buckets;
    unsigned int size;
    unsigned int n_occupied;
    unsigned int upper_bound;
    unsigned int scores;
    unsigned int *flags;
    char **keys;
    FASTQ **vals;
} FQDB;

/*! \struct SUFFIX_ARRAY
 *  \brief Data structure to hold suffix array
 */

typedef struct _suffix_array
{
    unsigned int n;        /*!< length of string including final null character */
    const char *string;    /*!< original string */
    const char **suffix;   /*!< suffix array of length n */
} SUFFIX_ARRAY;

/*! \struct parse_string
 *  \brief Data structure to hold parsed sequence coordinates
 */

struct parse_string
{
  char *chr;                /*!< chromosome or contig identifier */
  unsigned long int start;  /*!< base start position on chromosome */
  unsigned long int end;    /*!< base end position on chromosome */
};

/*! \struct COMPRESS_SEQ
 *  \brief Data structure to hold compressed IUPAC alphabet
 */

typedef struct _compressed_seq
{
    char *seq;
    unsigned int ulength;
    unsigned int clength;
} COMPRESS_SEQ;

/*! \struct ALIGN_RESULT
 *  \brief Data structure to hold the backtrace from alignment algorithm
 */

typedef struct _ksqr_t {
	int score;
	int target_begin;
	int target_end;
	int query_begin;
	int query_end;
	int score2;
	int target_end2;
} ALIGN_RESULT;

/*! \struct ALIGN_QUERY
 *  \brief Data structure to sequence alignment query
 */

typedef struct _kswq_t
{
    int qlen;
    int slen;
    unsigned char shift;
    unsigned char mdiff;
    unsigned char max;
    unsigned char size;
    __m128i *qp;
    __m128i *H0;
    __m128i *H1;
    __m128i *E;
    __m128i *Hmax;
} ALIGN_QUERY;

/*******************************************************************************
 * Globally scoped constants
 ******************************************************************************/

/* #define SCORE_NUM 0x1
 * FastQ scores are numeric
 */

#define SCORE_NUM 0x1

/* #define SCORE_ASCII 0x8
 * FastQ scores are ascii
 */

#define SCORE_ASCII 0x8

/* #define SCORE_SANGER 0x10
 * FastQ scores are Sanger scaled
 */

#define SCORE_SANGER 0x10

/* #define SCORE_ILLUMINA
 * FastQ scores are Illumina_1.3+ scaled
 */

#define SCORE_ILLUMINA 0x20

#define KSW_XBYTE  0x10000

#define KSW_XSTOP  0x20000

#define KSW_XSUBO  0x40000

#define KSW_XSTART 0x80000

#define MINUS_INF -0x40000000

/*******************************************************************************
 * Globally scoped variables
 ******************************************************************************/

extern char iupac_ext[];

/*******************************************************************************
 * Declare library API function prototypes
 ******************************************************************************/

/*******************************************************************************
 * File I/O functions
 ******************************************************************************/

/*! \fn STDDB *read_fasta (char *filename)
 *  \brief Read in fastA database from file
 *  \param filename Pointer to fastA input filename string
 *  \return Pointer to hash table on success, NULL on failure
*/

extern STDDB *read_fasta (char *filename);

/*! \fn FQDB *read_fastq (int num_files, ...)
 *  \brief Read in a fastQ database from one or two input files
 *  \param num_files The number of fastQ input files to read from disk
 *  \param ... list of pointers to input fastQ file names
 *  \return Pointer to hash table on success, NULL on failure
*/

extern FQDB *read_fastq (int num_files, ...);

/*! \fn int is_fasta (char *filename)
 *  \brief Determine if file is in fastA format
 *  \param filename Pointer to fastA input filename string
 *  \return Zero if file is not fastA and one if file is fastA
*/

extern int is_fasta (char *filename);

/*! \fn int is_fastq (char *filename)
 *  \brief Determine if file is in fastQ format
 *  \param filename Pointer to fastQ input filename string
 *  \return Zero if file is not fastQ and one if file is fastQ
*/

extern int is_fastq (char *filename);

/*! \fn int write_fasta (STDDB *h, const char *filename)
 *  \brief Write a fastA database to file
 *  \param h Pointer to fastA database
 *  \param filename Pointer to fastA output filename string
 *  \return Zero on success and non-zero on failure
*/

extern int write_fasta (STDDB *h, char *filename);

/*! \fn int write_fastq (FQDB *h, const char *filename)
 *  \brief Write a fastQ database to file
 *  \param h Pointer to fastQ database
 *  \param filename Pointer to fastQ output filename string
 *  \return Zero on success and non-zero on failure
*/

extern int write_fastq (FQDB *h, char *filename);

/*! \fn int write_fastq_paired (FQDB *h, const char *filename)
 *  \brief Write a paired-end fastQ database to file
 *  \param h Pointer to paired-end fastQ database
 *  \param filename Pointer to fastQ output filename string
 *  \return Zero on success and non-zero on failure
*/

extern int write_fastq_paired (FQDB *h, char *filename_base);

/*! \fn int write_fastq_paired (FQDB *h, const char *filename_base)
 *  \brief Write a paired-end fastQ database to two output files
 *  \param h Pointer to fastQ database
 *  \param filename_base Pointer to base fastQ output filename string
 *  \return Zero on success and non-zero on failure
*/

extern int write_fastq_paired (FQDB *h, char *filename_base);

/*! \fn int parse_region_string (char *string, struct parse_string *ps)
 *  \brief Parses a sequence region in the form <chr>:<start>-<end>
 *  If :<start>-<end> are omitted, the function assigns \a start = 1
 *  and \a end = ULONG_MAX, it is up to the programmer to use this information
 *  to check if a valid start and end position have been specified
 *  \param string Pointer to the region string to be parsed
 *  \param ps Pointer to the data structure to hold to coordinates
 *  \return Zero on success and non-zero on failure
*/

extern int parse_region_string (char *string, struct parse_string *ps);

/*! \fn int parse_illumina_id (FASTQ *f, char *line)
 *  \brief Parse illumina-formatted identifier line from fastQ entry
 *  \param f Pointer to FASTQ structure to hold parsed information
 *  \param line Pointer to the fastQ identifier string to be parsed
 *  \return Zero on success and non-zero on failure 
*/

extern int parse_illumina_id (FASTQ *f, char *line);

/*! \fn int parse_fastq_illumina (FQDB *h)
 *  \brief Parse fastQ entries by Illumina standard index
 *  \param string Pointer to the fastQ database
 *  \param adb Pointer to the adapter database
 *  \param outdir Pointer to string containing output directory
 *  \param num_threads Number of threads to run in pthreads
 *  \return Zero on success and non-zero on failure
*/

extern int parse_fastq_illumina (FQDB *h, STDDB *adb, char *outdir, int num_threads);

/*! \fn STDDB *create_adapter_database (char *filename)
 *  \brief Constructs database of adapter sequences from input CSV file
 *  \param filename Pointer to CSV adapter input filename string
 *  \return Pointer to hash table on success, NULL on failure
*/

extern STDDB *create_adapter_database (char *filename);

/*! \fn int pair_mates (FQDB *h)
 *  \brief Associates mated pair reads in a fastQ database
 *  \param h Pointer to the fastQ database
 *  \return Zero on success and non-zero on failure
*/

extern int pair_mates (FQDB *h);

/*! \fn int append_fastq_file (FASTQ *f, char *identifier, char *filename)
 *  \brief Write a single fastQ database entry to existing file
 *  \param f Pointer to a single fastQ database entry
 *  \param identifier Pointer to the identifier string (database key)
 *  \param filename Pointer to the output file name string
 *  \return Zero on success and non-zero on failure
*/

extern int append_fastq_file (FASTQ *f, char *identifier, gzFile *outfile);

/*******************************************************************************
 * Adding/Deleting database entries
 ******************************************************************************/

 /*! \fn FASTQ *fastq_init (void)
 *  \brief Initialize a FASTQ data structure
 *  \return Pointer to FASTQ data structure on success, NULL on failure
*/

extern FASTQ *fastq_init (void);

 /*! \fn int fastq_put_qual (FASTQ *f, char *qual)
 *  \brief Enter a quality string in the FASTQ data structure
 *  \param f Pointer to the fastQ data structure
 *  \param qual Pointer to the fastQ quality string
 *  \return Zero on success and non-zero on failure
*/

extern int fastq_put_qual (FASTQ *f, char *qual);

 /*! \fn int fastq_put_seq (FASTQ *f, char *seq)
 *  \brief Enter a sequence string in the FASTQ data structure
 *  \param f Pointer to the fastQ data structure
 *  \param seq Pointer to the fastQ sequence string
 *  \return Zero on success and non-zero on failure
*/

extern int fastq_put_seq (FASTQ *f, char *seq);

 /*! \fn int add_fasta_entry (STDDB *h, char *identifier, char *sequence)
 *  \brief Add a new entry to a fastA database
 *  \param h Pointer to the fastA database
 *  \param identifier Pointer to identifier of the new entry
 *  \param sequence Pointer to sequence of the new entry
 *  \return Zero on success and non-zero on failure
*/

extern int add_fasta_entry (STDDB *h, char *identifier, char *sequence);

 /*! \fn int add_fastq_entry (FQDB *h, char *identifier, char *sequence)
 *  \brief Add a new entry to a fastQ database
 *  \param h Pointer to the fastQ database
 *  \param identifier Pointer to identifier of the new entry
 *  \param sequence Pointer to sequence of the new entry
 *  \param qual Pointer to the sequence quality string
 *  \return Zero on success and non-zero on failure
*/

extern int add_fastq_entry (FQDB *h, char *identifier, char *sequence,
                            char *qual);

/*! \fn int delete_fasta_entry (STDDB *h, char *identifier)
 *  \brief Remove an existing entry from a fastA database
 *  \param h Pointer to the fastA database
 *  \param identifier Pointer to identifier of the entry to be deleted
 *  \return Zero on success and non-zero on failure
*/

extern int delete_fasta_entry (STDDB *h, char *identifier);

/*! \fn int delete_fastq_entry (FQDB *h, char *identifier)
 *  \brief Remove an existing entry from a fastQ database
 *  \param h Pointer to the fastA database
 *  \param identifier Pointer to identifier of the entry to be deleted
 *  \return Zero on success and non-zero on failure
*/

extern int delete_fastq_entry (FQDB *h, char *identifier);

/*******************************************************************************
 * Database properties
 ******************************************************************************/

/*! \fn int get_fasta_numseqs (STDDB *h)
 *  \brief Get the number of entries in fastA database
 *  \param h Pointer to the fastA database
 *  \return Integer of number of entries present in database
 */

extern int get_fasta_numseqs (STDDB *h);

/*! \fn int get_fastq_numseqs (FQDB *h)
 *  \brief Get the number of entries in fastQ database
 *  \param h Pointer to the fastQ database
 *  \return Integer of number of entries present in database
 */

extern int get_fastq_numseqs (FQDB *h);

/*! \fn char **get_fasta_seqnames (STDDB *h)
 *  \brief Get an array of entry identifiers
 *  \param h Pointer to the fastA database
 *  \return Pointer to array of sequence identifier strings
 */

extern char **get_fasta_seqnames (STDDB *h);

/*! \fn char **get_fastq_seqnames (FQDB *h)
 *  \brief Get an array of entry identifiers
 *  \param h Pointer to the fastQ database
 *  \return Pointer to array of sequence identifier strings
 */

extern char **get_fastq_seqnames (FQDB *h);


/*! \fn int is_paired (FQDB *h)
 *  \brief Determine if database comprises paired reads
 *  \param h Pointer to fastQ database
 *  \return Zero if database is not paired and one if database is paired
*/

extern int is_paired (FQDB *h);

/*******************************************************************************
 * Sequence entries
 ******************************************************************************/

/*! \fn char *fetch_fasta_seq (STDDB *h, char *query_string)
 *  \brief Fetchs the sequence of a fastA database entry
 *  \param h Pointer to fastA database
 *  \param query_string Pointer to query identifier string
 *  \return Pointer to sequence string
*/

extern char *fetch_fasta_seq (STDDB *h, char *query_string);

/*! \fn char *fetch_fastq_seq (FQDB *h, char *query_string)
 *  \brief Fetchs the sequence of a fastQ database entry
 *  \param h Pointer to fastQ database
 *  \param query_string Pointer to query identifier string
 *  \return Pointer to sequence string
*/

extern char *fetch_fastq_seq (FQDB *h, char *query_string);

/*! \fn char *fetch_fasta_subseq (STDDB *h, char *identifier, unsigned long int start, unsigned long int end)
 *  \brief Fetchs a substring from a sequence in a fastA database entry
 *  \param h Pointer to fastA database
 *  \param identifier Pointer to query identifier string
 *  \param start First position in substring
 *  \param end End position in substring
 *  \return Pointer to sequence string
*/

extern char *fetch_fasta_subseq (STDDB *h, char *identifier, unsigned long int start,
                                 unsigned long int end);
                                 
/*! \fn char *fetch_fastq_subseq (FQDB *h, char *identifier, unsigned long int start, unsigned long int end)
 *  \brief Fetchs a substring from a sequence in a fastQ database entry
 *  \param h Pointer to fastQ database
 *  \param identifier Pointer to query identifier string
 *  \param start First position in substring
 *  \param end End position in substring
 *  \return Pointer to sequence string
*/

extern char *fetch_fastq_subseq (FQDB *h, char *identifier, unsigned long int start,
                                 unsigned long int end);

/*! \fn size_t get_fasta_seqlength (STDDB *h, char *identifier)
 *  \brief Get the length of a fastA sequence entry
 *  \param h Pointer to the fastA database
 *  \param identifier A pointer to the query identifier string
 *  \return Length of sequence on success and zero on failure
 */

extern size_t get_fasta_seqlength (STDDB *h, char *identifier);

/*! \fn size_t get_fastq_seqlength (FQDB *h, char *identifier)
 *  \brief Get the length of a fastQ sequence entry
 *  \param h Pointer to the fastQ database
 *  \param identifier A pointer to the query identifier string
 *  \return Length of sequence on success and zero on failure
 */

extern size_t get_fastq_seqlength (FQDB *h, char *identifier);

/*! \fn char *revcom (char *input_string)
 *  \brief Provides reverse complement of DNA string
 *  \param input_string Pointer to string to reverse complement
 *  \return Pointer to new memory address with reverse complement string
*/

extern char *revcom (char *input_string);

/*! \fn char *compress_seq (char *input_string)
 *  \brief Compresses IUPAC alphabet into 4-bit encoding
 *  \param s Pointer to string to compress
 *  \return Pointer to new memory address with 4-bit encoded string
*/

extern COMPRESS_SEQ *compress_seq (char *s);

/*! \fn char *uncompress_seq (char *input_string)
 *  \brief Uncompresses 4-bit encoding of IUPAC alphabet
 *  \param cs Pointer to compressed sequence data structure to uncompress
 *  \return Pointer to new memory address with uncompressed IUPAC string
*/

extern char *uncompress_seq (COMPRESS_SEQ *cs);

/*******************************************************************************
 * Quality scores
 ******************************************************************************/

/*! \fn char *fetch_qual (FQDB *h, char *query_string)
 *  \brief Fetchs the quality string of a fastQ database entry
 *  \param h Pointer to fastQ database
 *  \param query_string Pointer to query identifier string
 *  \return Pointer to sequence string
*/

extern char *fetch_qual (FQDB *h, char *query_string);

/*! \fn char *fetch_subqual (FQDB *h, char *identifier, unsigned long int start, unsigned long int end)
 *  \brief Fetchs a substring from a quality string in a fastQ database entry
 *  \param h Pointer to fastQ database
 *  \param identifier Pointer to query identifier string
 *  \param start First position in substring
 *  \param end End position in substring
 *  \return Pointer to sequence string
*/

extern char *fetch_subqual (FQDB *h, char *identifier, unsigned long int start,
                            unsigned long int end);

/*! \fn int get_fastq_score (FQDB *h)
 *  \brief Determine the scale of quality scores in a fastQ database
 *  \param h Pointer to fastQ database
 *  \return Integer that maps to the scale of the quality scores
*/

extern int get_fastq_score (FQDB *h);

/*! \fn double *avg_qual_bypos (FQDB *h)
 *  \brief Calculates average quality by position for all entries in fastQ database
 *  \param h Pointer to fastQ database
 *  \return Pointer to array of floating point qualities
*/

extern double *avg_qual_bypos (FQDB *h);

/*! \fn int convert_fastq_score (FQDB *h, int orig_score, int new_score)
 *  \brief Convert the scale of quality scores in a fastQ database
 *  \param h Pointer to fastQ database
 *  \param orig_score The original scale of quality scores in the fastQ database
 *  \param new_score The desired scale of quality scores in the fastQ database
 *  \return Zero on success and non-zero on failure
*/

extern int convert_fastq_score (FQDB *h, int orig_score, int new_score);

/*******************************************************************************
 * Filtering/Cleaning entries
 ******************************************************************************/

/*! \fn int filter_fastq (FQDB *h, double propN)
 *  \brief Filter reads in a fastQ database based on the proportion of ambiguous characters
 *  \param h Pointer to fastQ database
 *  \param propN Allowable proportion of ambiguous base calls
 *  \return Zero on success and non-zero on failure
*/

extern int filter_fastq (FQDB *h, double propN);

/*! \fn int filter_fastq_paired (FQDB *h, double propN)
 *  \brief Filter paired reads in a fastQ database based on the proportion of ambiguous characters
 *  \param h Pointer to fastQ database
 *  \param propN Allowable proportion of ambiguous base calls
 *  \return Zero on success and non-zero on failure
*/

extern int filter_fastq_paired (FQDB *h, double propN);

/*! \fn int trim_fastq (FQDB *h, int min_length)
 *  \brief Trim the end of reads in a fastQ database
 *  \param h Pointer to fastQ database
 *  \param trim_quality Trim quality threshold parameter
 *  \param min_length Minimum length of reads to keep after trimming
 *  \return Zero on success and non-zero on failure
*/

extern int trim_fastq (FQDB *h, int trim_quality, int min_length);

/*! \fn int trim_fastq_paired (FQDB *h, int min_length)
 *  \brief Trim the end of paired reads in a fastQ database
 *  \param h Pointer to fastQ database
 *  \param trim_quality Trim quality threshold parameter
 *  \param min_length Minimum length of reads to keep after trimming
 *  \return Zero on success and non-zero on failure
*/

extern int trim_fastq_paired (FQDB *h, int trim_quality, int min_length);

/*! \fn int trim_adapter_seq (FQDB *h, STDDB *adb, int orient, int edit_dist)
 *  \brief Trim adapter sequences from either 3' or 5' end of database entry
 *  \param h Pointer to fastQ database
 *  \param adb Pointer to the adapter sequence database
 *  \param orient 1 for forward reads, 2 for reverse reads
 *  \param edit_dist Minimum edit distance to consider a match
 *  \return Zero on success and non-zero on failure
*/

extern int trim_adapter_seq (FQDB *h, STDDB *adb, int orient, int edit_dist);

/*******************************************************************************
 * Database search functions
 ******************************************************************************/

/*! \fn SUFFIX_ARRAY *suffix_array_create (const char *string)
 *  \brief Construct the suffix array
 *  \param string Pointer to string constituting the suffix array
 *  \return Pointer to the suffix array data structure
 */

extern SUFFIX_ARRAY *suffix_array_create (const char *string);

/*! \fn void suffix_array_destroy (SUFFIX_ARRAY *sa)
 *  \brief Deallocate memory for suffix array
 *  \param sa Pointer to suffix array data structre
 */

extern void suffix_array_destroy (SUFFIX_ARRAY *sa);

/*! \fn unsigned int suffix_array_search (SUFFIX_ARRAY *sa, const char *substring, unsigned int *first)
 *  \brief Search the suffix array for the number of occurrences of a substring
 *  \param sa Pointer to the suffix array data structure
 *  \param substring Pointer to the query string
 *  \param first Pointer to positions of each occurrence
 *  \return Number of occurrences of query string
 */

extern unsigned int suffix_array_search (SUFFIX_ARRAY *sa, const char *substring, unsigned int *first);

/*! \fn char *suffix_array_BWT (SUFFIX_ARRAY *sa)
 *  \brief Get Burrows-Wheeler transform of underlying string
 *  \param sa Pointer to the suffix array data structure
 *  \return Pointer to new memory holding Burrows-Wheeler transform of suffix array
 */

extern char *suffix_array_BWT (SUFFIX_ARRAY *sa);

/*! \fn char *inverse_BWT (unsigned int len, const char *s)
 *  \brief Invert BWT of null-terminated string
 *  \param len
 *  \param s
 *  \return Pointer to new memory containing original string
 */

extern char *inverse_BWT (unsigned int len, const char *s);

/*! \fn int levenshtein (char *s1, char *s2)
 *  \brief Calculates the Levenshtein distance between two strings
 *  \param s1 Pointer to first input string
 *  \param s2 Pointer to second input string
 *  \return Integer representing edit distance between the two strings
 */

extern int levenshtein (char *s1, char *s2);

/*******************************************************************************
 * Sequence alignment functions
 ******************************************************************************/

extern int global_align (int qlen, const unsigned char *query, int tlen, 
              const unsigned char *target, int m, const char *mat,
              int gapo, int gape, int w, int *n_cigar_,
              unsigned int **cigar_);

extern ALIGN_RESULT local_align (int qlen, unsigned char *query, int tlen,
           unsigned char *target, int m, const char *mat, int gapo,
           int gape, int xtra, ALIGN_QUERY **qry);

/*******************************************************************************
 * Hashing macros
 ******************************************************************************/

#define hash_value(h, x) ((h)->vals[x])

#define hash_key(h, x) ((h)->keys[x])

#define hash_begin(h) (unsigned int)(0)

#define hash_end(h) ((h)->n_buckets)

#define hash_size(h) ((h)->size)

#define fqdb_value(h, x) ((h)->vals[x])

#define fqdb_key(h, x) ((h)->keys[x])

#define fqdb_begin(h) (unsigned int)(0)

#define fqdb_end(h) ((h)->n_buckets)

#define fqdb_size(h) ((h)->size)

/*******************************************************************************
 * Hashing functions
 ******************************************************************************/

extern STDDB *hash_init (void);

extern FQDB *fqdb_init (void);

extern void hash_destroy (STDDB *h);

extern void fqdb_destroy (FQDB *h);

extern void hash_clear (STDDB *h);

extern void fadq_clear (FQDB *h);

extern unsigned int hash_get (STDDB *h, char *key);

extern unsigned int fqdb_get (FQDB *h, char *key);

extern int hash_resize (STDDB *h, unsigned int new_n_buckets);

extern int fqdb_resize (FQDB *h, unsigned int new_n_buckets);

extern unsigned int hash_put (STDDB *h, char *key, int *ret);

extern unsigned int fqdb_put (FQDB *h, char *key, int *ret);

extern void hash_delete (STDDB *h, unsigned int x);

extern void fqdb_delete (FQDB *h, unsigned int x);

extern int hash_exists (STDDB *h, unsigned int x);

extern int fqdb_exists (FQDB *h, unsigned int x);

#ifdef __cplusplus
}
#endif

#endif
