/*! \file filter_fastq.c
 *  \brief Filter reads in a fastQ database based on the proportion of ambiguous characters
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
filter_fastq (FQDB *h, double propN)
{
    int numN = 0;
    int countN = 0;
    size_t i = 0;
    size_t seqlen = 0;
    unsigned int k = 0;
    FASTQ *f;

    /* Iterate through the database */
    for (k = fqdb_begin(h); k!= fqdb_end(h); k++)
        {
            if (fqdb_exists(h, k))
                {
					f = fqdb_value(h, k);
                    seqlen = strlen (f->seq);
                    numN = (int)(seqlen * propN);
                    countN = 0;

                    /* Count number of ambiguous characters */
                    for (i = 0; i < seqlen; i++)
                        {
                            if (f->seq[i] == 'N')
                                {
                                    countN++;
                                }
                        }

                    /* If threshold is exceed, delete the entry */
                    if (countN > numN)
                        {
                            delete_fastq_entry (h, fqdb_key(h, k));
                        }
                }
        }

    return 0;
}
