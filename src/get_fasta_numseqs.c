/*! \file get_fasta_numseqs.c
 *  \brief Get the number of entries in fastA database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include "fasta.h"

int
get_fasta_numseqs (STDDB *h)
{
    return hash_size(h);
}
