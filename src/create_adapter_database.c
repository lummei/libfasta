/*! \file create_adapter_database.c
 *  \brief Constructs database of adapter sequences from input CSV file
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include "fasta.h"

#ifndef MAX_LINE_LENGTH
#define MAX_LINE_LENGTH 400
#endif

STDDB *
create_adapter_database (char *filename)
{
    int ret = 0;
    unsigned int k = 0;
    size_t token_length = 0;
    char buf[MAX_LINE_LENGTH];
    char seps[] = ",";
    char *tok = NULL;
    char *reserve = NULL;
    char *adapt_seq = NULL;
    char *adapt_label = NULL;
    STDDB *adb;
    gzFile infile;

    /* Initialize adaptor sequence hash table */
    adb = hash_init ();

    /* Open input adaptor text file stream */
    if ((infile = gzopen (filename, "rb")) == Z_NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the input "
                     "CSV file: %s\n", __func__, __LINE__, filename);
            return NULL;
        }

    /* Enter data from the text adaptor input file into the database */
    while (gzgets (infile, buf, MAX_LINE_LENGTH) != Z_NULL)
		{
			/* Parse the CSV line */
			tok = strtok_r (buf, seps, &reserve);
			if (tok == NULL)
				{
					fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
							 "parse entries from CSV input file: %s\n",
							 __func__, __LINE__, filename);
					return NULL;
				}

			/* Enter key into hash table */
			token_length = strlen (tok);
			if ((adapt_seq = malloc (token_length + 1u)) == NULL)
				{
					fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
							 "memory for adapt_seq string\n", __func__,
							 __LINE__);
					return NULL;
				}
			strcpy (adapt_seq, tok);
			k = hash_put (adb, adapt_seq, &ret);

			/* Add value to hash tuple */
			tok = strtok_r (NULL, seps, &reserve);
			if (tok == NULL)
				{
					fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
							 "parse entries from CSV input file: %s\n",
							 __func__, __LINE__, filename);
					return NULL;
				}			
			token_length = strcspn (tok, "\n");
			if ((adapt_label = malloc (token_length + 1u)) == NULL)
				{
					fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
							 "memory for adapt_label string\n", __func__,
							 __LINE__);
					return NULL;
				}
			strncpy (adapt_label, tok, token_length);
			adapt_label[token_length] = '\0';
			hash_value(adb, k) = adapt_label;
        }   /* End of adapter text input file */

    /* Close input CSV file stream */
    gzclose (infile);

    /* Return pointer to adapter sequence database */
    return adb;
}
