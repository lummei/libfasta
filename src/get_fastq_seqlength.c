/*! \file get_fastq_seqlength.c
 *  \brief Get the length of a fastQ sequence entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <string.h>
#include "fasta.h"

size_t
get_fastq_seqlength (FQDB *h, char *identifier)
{
    unsigned int k = fqdb_get (h, identifier);
    FASTQ *f;

    /* Lookup the query key from database */
    if (k < fqdb_end(h))
        {
			f = fqdb_value(h, k);
            return strlen (f->seq);
        }
    else
        {
            return 0;
        }
}
