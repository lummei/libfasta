/*! \file parse_fastq_illumina.c
 *  \brief Parse fastQ entries by Illumina standard index
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <zlib.h>
#include "fasta.h"

struct thread_data
{
	int num_indices;
	unsigned int start;
	unsigned int end;
	unsigned int *map;
	STDDB *adb;
	FQDB *db;
	gzFile *out;
};

pthread_mutex_t lock;

void *thread_parse(void *data);

int
parse_fastq_illumina (FQDB *h, STDDB *adb, char *outdir, int num_threads)
{
	int i = 0;
	int j = 0;
	int dir_writable = 0;
	unsigned int k = 0;
	unsigned int num_indices;
	unsigned int *map = NULL;
	char **outfile_names = NULL;
	char *val = NULL;
	struct thread_data *data;
	gzFile *out;

	/* Check if output directory is writable */
	if ((dir_writable = access (outdir, W_OK)) != 0)
		{
			fprintf (stderr, "[libfasta:%s:%d] Error: process does not "
			         "have write permissions on output directory %s.\n",
			         __func__, __LINE__, outdir);
			return -1;
		}

	/* Calculate number of indices in database */
	num_indices = hash_size(adb);

	/* Allocate and populate output file names */
    if ((outfile_names = malloc (2 * num_indices * sizeof (char *))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                     "memory for outfile names.\n", __func__, __LINE__);
            return -1;
        }
    if ((map = malloc (num_indices * sizeof (unsigned int))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                     "memory for map.\n", __func__, __LINE__);
            return -1;
        }
    if ((out = malloc (2 * num_indices * sizeof (gzFile))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                     "memory for outfile handles.\n", __func__, __LINE__);
            return -1;
        }

	for (k = hash_begin(adb), i = 0, j = 0; k != hash_end(adb); k++)
		{
			if (hash_exists(adb, k))
				{
					val = hash_value(adb, k);

					/* Do forward orientation first */
					if ((outfile_names[i] = malloc (strlen (outdir) + strlen (val) + 16)) == NULL)
						{
							fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
									 "memory for outfile name.\n", __func__, __LINE__);
							return -1;
						}
					sprintf (outfile_names[i], "%spool_%s.R1.fq.gz", outdir, val);
					map[j] = k;
					if ((out[i] = gzopen (outfile_names[i], "ab")) == Z_NULL)
						{
							fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
									 "open output fastQ file: %s.\n", __func__, 
									 __LINE__, outfile_names[i]);
							return -1;
						}
					++i;

					/* Do reverse orientation next */
					if ((outfile_names[i] = malloc (strlen (outdir) + strlen (val) + 16)) == NULL)
						{
							fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
									 "memory for outfile name\n", __func__, __LINE__);
							return -1;
						}
					sprintf (outfile_names[i], "%spool_%s.R2.fq.gz", outdir, val);
					if ((out[i] = gzopen (outfile_names[i], "ab")) == Z_NULL)
						{
							fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
									 "open output fastQ file: %s.\n", __func__,
									 __LINE__, outfile_names[i]);
							return -1;
						}
					++i;
					++j;
				}
		}

	unsigned int fsize = fqdb_end(h);
	unsigned int records_per_thread = (num_threads + fsize - 1) / num_threads;
	pthread_t *thr;
	thr = malloc (num_threads * sizeof (pthread_t));
	data = malloc (num_threads * sizeof (struct thread_data));

	for (i = 0; i < num_threads; i++)
		{
			data[i].num_indices = num_indices;
			data[i].start = i * records_per_thread;
			data[i].end = (i + 1) * records_per_thread;
			data[i].map = &map[0];
			data[i].db = h;
			data[i].out = &out[0];
			data[i].adb = adb;
		}
	data[num_threads - 1].end = fsize;

	for (i = 0; i < num_threads; i++)
		{
			pthread_create (thr+i, NULL, thread_parse, (void*)(&data[i]));
		}
	for (i = 0; i < num_threads; i++)
		{
			pthread_join (thr[i], NULL);
		}

	/* Free allocated memory and close output file streams*/
	for (i = 0; i < 2 * num_indices; i++)
		{
			gzclose (out[i]);
			free (outfile_names[i]);
		}
	free (data);
	free (thr);
	free (out);
	free (outfile_names);
	free (map);

	return 0;
}

void *thread_parse(void *data)
{
	int i = 0;
	int index = 0;
	int is_reverse = 0;
	unsigned int k = 0;
	unsigned int r = 0;
	char *key = NULL;
	struct thread_data *d = (struct thread_data *)data;
	FASTQ *f = NULL;

    /* Iterate through the fastQ database and check Illumina indices */
    for (k = d->start; k < d->end; k++)
        {
            if (fqdb_exists(d->db, k))
                {
					f = fqdb_value(d->db, k);
					is_reverse = (f->read == 1) ? 0 : 1;
					key = fqdb_key(d->db, k);
					r = hash_get(d->adb, f->index_sequence);
					for (i = 0; i < d->num_indices; i++)
						{
							if (r == d->map[i])
								{
									if (is_reverse)
										{
											index = (2 * i) + 1;
										}
									else
										{
											index = 2 * i;
										}
									pthread_mutex_lock(&lock);
                                    gzputc (d->out[index], '@');
                                    gzputs (d->out[index], key);
									gzputc (d->out[index], '\n');
									gzputs (d->out[index], f->seq);
									gzputs (d->out[index], "\n+\n");
									gzputs (d->out[index], f->qual);
									gzputc (d->out[index], '\n');
									pthread_mutex_unlock(&lock);
									break;
								}
						}
				}
		}
}
