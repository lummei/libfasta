/*! \file parse_illumina_id.c
 *  \brief Parse illumina-formatted (Casava 1.8) identifier line from fastQ entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
parse_illumina_id (FASTQ *f, char *line)
{
    char seps[] = ": ";
    char *tok = NULL;
    char *cp = NULL;
    char *reserve = NULL;
    size_t token_length = 0;

    /* Parse if string is not null */
    if (line != NULL)
        {
            if ((cp = malloc (strlen (line) + 1u)) == NULL)
				{
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                             "memory for copy of illumina string\n", __func__,
                             __LINE__);
                    return -1;
				}

            /* Make a local copy of the string */
            strcpy (cp, line);

            /* Parse instrument identifier */
            tok = strtok_r (cp, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error cannot parse "
                             "instrument identifer line\n", __func__,
                             __LINE__);
                    return -1;
                }
            token_length = strlen (tok);

            if ((f->instrument = malloc (token_length + 1u)) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                             "memory for iids instrument string\n", __func__,
                             __LINE__);
                    return -1;
                }

            strcpy (f->instrument, tok);

            /* Parse run number */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "run number\n", __func__, __LINE__);
                    fprintf (stderr, "%s\n", line);
                    return -1;
                }
            f->run_number = atoi (tok);

            /* Parse flowcell ID */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "flowcell ID\n", __func__, __LINE__);
                    return -1;
                }
            token_length = strlen (tok);

            if ((f->flowcell_ID = malloc (token_length + 1u)) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                             "memory for iids flowcell ID string\n", __func__,
                             __LINE__);
                    return -1;
                }

            strcpy (f->flowcell_ID, tok);

            /* Parse lane number */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "lane number\n", __func__, __LINE__);
                    return -1;
                }
            f->lane = atoi (tok);

            /* Parse tile number */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "tile number\n", __func__, __LINE__);
                    return -1;
                }
            f->tile = atoi (tok);

            /* Parse X coordinate */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "X coordinate\n", __func__, __LINE__);
                    return -1;
                }
            f->x_pos = atoi (tok);

            /* Parse Y coordinate */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "Y coordinate\n", __func__, __LINE__);
                    return -1;
                }
            f->y_pos = atoi (tok);

            /* Parse read number */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "read number\n", __func__, __LINE__);
                    return -1;
                };
            f->read = atoi (tok);

            /* Parse is_filtered */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "is_filtered\n", __func__, __LINE__);
                    return -1;
                }
            f->is_filtered = atoi (tok);

            /* Parse control number */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "control number\n", __func__, __LINE__);
                    return -1;
                }
            f->control_number = atoi (tok);

            /* Parse index sequence */
            tok = strtok_r (NULL, seps, &reserve);
            if (tok == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot parse "
                             "index sequence\n", __func__, __LINE__);
                    return -1;
                }
            token_length = strlen (tok);

            if ((f->index_sequence = malloc (token_length + 1u)) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                             "memory for iids index string\n", __func__,
                             __LINE__);
                    return -1;
                }

            strcpy (f->index_sequence, tok);
            free (cp);
        }
    else
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: need to specify an "
                             "identifier line", __func__, __LINE__);
            return -1;
        }

	return 0;
}
