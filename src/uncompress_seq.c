/*! \file uncompress_seq.c
 *  \brief Uncompresses 4-bit encoding of IUPAC alphabet
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

char *
uncompress_seq (COMPRESS_SEQ *cs)
{
    unsigned short int c = 0;
    unsigned int j = 0;
    char *head = NULL;
    char *end = NULL;
    char *uncomp_str = NULL;

    /* Allocate memory for uncompressed sequence string */
    if ((uncomp_str = malloc ((cs->ulength + 1u) * sizeof (char))) == NULL)
        {
            fputs ("Error: failure to allocate memory for uncompressed_string.", stderr);
            return NULL;
        }

    /* Iterate through compressed sequence and deflate to one character per byte */
    for (head = cs->seq, end = cs->seq + cs->clength - 1, j = 0; head <= end; j++)
        {
            c = (unsigned short int)(*head);
            if (j % 2)
                {
                    uncomp_str[j] = iupac_ext[(c & 0xf0) >> 4];
                    if (head == end)
                        {
                            uncomp_str[++j] = '\0';
                            break;
                        }
                    else
                        {
                            ++head;
                        }
                }
            else
                {
                    uncomp_str[j] = iupac_ext[(c & 0xf)];
                    if ((cs->ulength % 2) && (head == end))
                        {
                            uncomp_str[++j] = '\0';
                            break;
                        }
                }
        }

    return uncomp_str;
}
