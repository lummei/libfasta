/*! \file get_fasta_seqlength.c
 *  \brief Get the length of a fastA sequence entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <string.h>
#include "fasta.h"

size_t
get_fasta_seqlength (STDDB *h, char *identifier)
{
    unsigned int k = hash_get (h, identifier);

    /* Lookup the query key from database */
    if (k < hash_end(h))
        {
            return strlen (hash_value(h, k));
        }
    else
        {
            return 0;
        }
}
