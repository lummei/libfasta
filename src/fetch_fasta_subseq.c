/*! \file fetch_fasta_subseq.c
 *  \brief Fetch a subsequence from a fastA database entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

char *
fetch_fasta_subseq (STDDB *h, char *identifier, unsigned long int start, unsigned long int end)
{
    unsigned int k = 0;
    size_t size = 0;
    char *substring = NULL;
    char *startptr = NULL;

    /* Lookup the query key from database */
    k = hash_get (h, identifier);

    if (k < hash_end(h))
        {
            if ((start < 1) || (end > size))
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: requested start and end "
                                     "positions are out of bounds\n", __func__, __LINE__);
                    return NULL;
                }
            else
                {
                    size = end - start;

                    if ((substring = malloc ((size + 1) * sizeof (char))) == NULL)
                        {
                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                     "memory for substring\n", __func__, __LINE__);
                            return NULL;
                        }

                    startptr = (hash_value(h, k)) + (start - 1);
                    strncpy (substring, startptr, size);
                    substring[size] = '\0';
                    return substring;
                }
        }
    else
        {
            return NULL;
        }
}
