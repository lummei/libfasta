/*! \file read_fasta.c
 *  \brief Read in fastA database from file
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include "fasta.h"

/*! \def BLOCK_SIZE
*  \brief The size of block to be read from disk (128 kb)
*/

#define BLOCK_SIZE 0x20000

/*! \def PARSE_BLOCK_SIZE
 *  \brief The size of buffered block to be parsed
 */

#define PARSE_BLOCK_SIZE 1000

/*! \def PARSE_INIT
 *  \brief The initial state of the parser
 */

#define PARSE_INIT 0

/*! \def PARSE_ID
 *  \brief The parser is currently in an identifier string
 */

#define PARSE_ID 1

/*! \def PARSE_SEQ
 *  \brief The parser is currently in a sequence string
 */

#define PARSE_SEQ 2

/*! \def PARSE_TRANS
 *  \brief The parser is currently on a transition character
 */

#define PARSE_TRANS 3

STDDB *
read_fasta (char *filename)
{
    int i = 0;
    int ret = 0;
    int idlen = 0;
    int curr_id_limit = PARSE_BLOCK_SIZE;
    int state = PARSE_INIT;
    int nseqs = 0;
    int seqidx = 0;
    unsigned long int curr_seq_limit = PARSE_BLOCK_SIZE;
    unsigned long int *lenptr1 = NULL;
    unsigned long int *lenptr2 = NULL;
    char buf[BLOCK_SIZE];
    char **idptr1 = NULL;
    char **idptr2 = NULL;
    char **seqptr1 = NULL;
    char **seqptr2 = NULL;
    char *gptr = NULL;
    gzFile infile;
    unsigned int k;
    STDDB *h;

    h = hash_init();

    /* Open input fastA file stream */
    if ((infile = gzopen (filename, "rb")) == Z_NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the input "
                     "file: %s\n", __func__, __LINE__, filename);
            return NULL;
        }

    /* Read blocks into memory */
    while ((ret = gzread (infile, buf, BLOCK_SIZE)) != 0)
        {
            for (i = 0; i < ret; i++)
                {
                    if ((buf[i] == '>') && (state == PARSE_INIT))
                        {
                            state = PARSE_ID;
                            idlen = 0;
                            nseqs = 1;
                            seqidx = 0;

                            if ((lenptr1 = malloc (sizeof (unsigned long int))) == NULL)
                                {
                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                             "memory for length pointer\n", __func__, __LINE__);
                                    return NULL;
                                }

                            if ((idptr1 = malloc (sizeof (char *))) == NULL)
                                {
                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                             "memory for id pointer 1\n", __func__, __LINE__);
                                    return NULL;
                                }

                            if ((idptr1[seqidx] = malloc (PARSE_BLOCK_SIZE * sizeof (char))) == NULL)
                                {
                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                             "memory for first id pointer\n", __func__, __LINE__);
                                    return NULL;
                                }

                            if ((seqptr1 = malloc (sizeof (char *))) == NULL)
                                {
                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                             "memory for sequence pointer\n", __func__, __LINE__);
                                    return NULL;
                                }

                            if ((seqptr1[seqidx] = malloc (PARSE_BLOCK_SIZE * sizeof (char))) == NULL)
                                {
                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                             "memory for first sequence pointer\n", __func__, __LINE__);
                                    return NULL;
                                }

                            lenptr1[seqidx] = 0;
                        }
                    else if (state == PARSE_ID)
                        {
                            if (buf[i] == '\n')
                                {
                                    idptr1[seqidx][idlen] = '\0';
                                    state = PARSE_SEQ;
                                }
                            else
                                {
                                    if (idlen >= curr_id_limit)
                                        {
                                            curr_id_limit += PARSE_BLOCK_SIZE;
                                            if ((gptr = realloc (idptr1[seqidx], curr_id_limit * sizeof (char))) == NULL)
                                                {
                                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                                                             "memory for id pointer\n", __func__, __LINE__);
                                                    return NULL;
                                                }
                                            else
                                                {
                                                    idptr1[seqidx] = gptr;
                                                }
                                        }
                                    idptr1[seqidx][idlen] = buf[i];
                                    idlen++;
                                }
                        }
                    else if (state == PARSE_SEQ)
                        {
                            if (buf[i] == '\n')
                                {
                                    seqptr1[seqidx][lenptr1[seqidx]] = '\0';
                                    state = PARSE_TRANS;
                                }
                            else
                                {
                                    if (lenptr1[seqidx] >= curr_seq_limit)
                                        {
                                            curr_seq_limit += PARSE_BLOCK_SIZE;
                                            if ((gptr = realloc (seqptr1[seqidx], curr_seq_limit * sizeof (char))) == NULL)
                                                {
                                                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                                                             "memory for sequence pointer\n", __func__, __LINE__);
                                                    return NULL;
                                                }
                                            else
                                                {
                                                    seqptr1[seqidx] = gptr;
                                                }
                                        }
                                    seqptr1[seqidx][lenptr1[seqidx]] = buf[i];
                                    lenptr1[seqidx]++;
                                }
                        }
                    else if (state == PARSE_TRANS)
                        {
                            if (buf[i] == '>')
                                {
                                    state = PARSE_ID;
                                    idlen = 0;
                                    curr_id_limit = PARSE_BLOCK_SIZE;
                                    curr_seq_limit = PARSE_BLOCK_SIZE;
                                    nseqs++;
                                    seqidx = nseqs - 1;

                                    if ((lenptr2 = realloc (lenptr1, nseqs * sizeof (unsigned long int))) == NULL)
                                        {
                                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                                                     "memory for length pointer\n", __func__, __LINE__);
                                            return NULL;
                                        }
                                    else
                                        {
                                            lenptr1 = lenptr2;
                                        }

                                    if ((idptr2 = realloc (idptr1, nseqs * sizeof (char *))) == NULL)
                                        {
                                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                                                     "memory for id pointer\n", __func__, __LINE__);
                                            return NULL;
                                        }
                                    else
                                        {
                                            idptr1 = idptr2;
                                        }

                                    if ((idptr1[seqidx] = malloc (curr_id_limit * sizeof (char))) == NULL)
                                        {
                                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                                     "memory for next id pointer\n", __func__, __LINE__);
                                            return NULL;
                                        }

                                    if ((seqptr2 = realloc (seqptr1, nseqs * sizeof (char *))) == NULL)
                                        {
                                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                                                     "memory for sequence pointer\n", __func__, __LINE__);
                                            return NULL;
                                        }
                                    else
                                        {
                                            seqptr1 = seqptr2;
                                        }

                                    if ((seqptr1[seqidx] = malloc (curr_seq_limit * sizeof (char))) == NULL)
                                        {
                                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                                     "memory for next sequence pointer\n", __func__, __LINE__);
                                            return NULL;
                                        }
                                    lenptr1[seqidx] = 0;
                                }
                            else
                                {
                                    seqptr1[seqidx][lenptr1[seqidx]] = buf[i];
                                    lenptr1[seqidx]++;
                                    state = PARSE_SEQ;
                                }
                        }
                }          /* No more characters in buffer */
        }          /* No more characters in file stream */

    gzclose (infile);

    /* Free unused memory */
    for (i = 0; i < nseqs; i++)
        {
            idlen = strlen (idptr1[i]) + 1;

            if ((gptr = realloc (idptr1[i], idlen * sizeof (char))) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                             "memory for id pointer\n", __func__, __LINE__);
                    return NULL;
                }
            else
                {
                    idptr1[i] = gptr;
                }

            idlen = strlen (seqptr1[i]) + 1;

            if ((gptr = realloc (seqptr1[i], idlen * sizeof (char))) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                             "memory for sequence pointer\n", __func__, __LINE__);
                    return NULL;
                }
            else
                {
                    seqptr1[i] = gptr;
                }
        }

    /* Populate the database from parsed fastA file data */
    for (i = 0; i < nseqs; i++)
        {
            k = hash_put (h, strdup (idptr1[i]), &ret);
            hash_value(h, k) = strdup (seqptr1[i]);
            free (seqptr1[i]);
            free (idptr1[i]);
        }

    free (idptr1);
    free (lenptr1);
    free (seqptr1);;

    return h;
}
