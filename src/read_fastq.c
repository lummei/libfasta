/*! \file read_fastq.c
 *  \brief Read in a fastQ database from one or two input files
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <zlib.h>
#include "fasta.h"

#ifndef MAX_LINE_LENGTH
#define MAX_LINE_LENGTH 400
#endif
#ifndef BUFFSIZE
#define BUFFSIZE 4000
#endif

FQDB *
read_fastq (int num_files, ...)
{
    int i = 0;
    int j = 0;
    int lc = 0;
    size_t pos = 0;
    char buf[BUFFSIZE][MAX_LINE_LENGTH];
    char *filename = NULL;
    va_list ap;
    gzFile fq;
    FQDB *h;

    h = fqdb_init ();

    /* Check number of infile arguments */
    if ((num_files > 2) || (num_files < 1))
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: please provide one or two "
                     "input filenames\n", __func__, __LINE__);
            return NULL;
        }

    /* Initialize variable argument structure */
    va_start (ap, num_files);


    /* Iterate through function arguments */
    for (i = 0; i < num_files; i++)
        {
            /* Get input filename */
            if ((filename = strdup (va_arg (ap, char *))) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate memory "
                             "for filename string\n", __func__, __LINE__);
                    return NULL;
                }

            /* Open the fastQ input stream */
            if ((fq = gzopen (filename, "rb")) == Z_NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the input "
                             "fastQ file %s\n", __func__, __LINE__, filename);
                    return NULL;
                }

            /* Free alloc'ed memory for infile name */
            free (filename);

            /* Enter data from the fastQ input file into the database */
            while (1)
                {
                    /* Fill up the buffer */
                    for (lc = 0; lc < BUFFSIZE; lc++)
                        {
                            /* Clear the buffer */
                            memset (buf[lc], 0, MAX_LINE_LENGTH);

                            /* Get line from the fastQ input stream */
                            if (gzgets (fq, buf[lc], MAX_LINE_LENGTH) == Z_NULL)
                                {
                                    break;
                                }
                        }

                    /* Iterate through lines in the buffer */
                    for (j = 0; j < lc; j++)
                        {
                            /* We are at the end of one fastQ entry */
                            if (j % 4 == 3)
                                {
                                    /* Process entry identifier */
                                    pos = strcspn (buf[j-3], "\n");
                                    buf[j-3][pos] = '\0';
                                    pos = strcspn (buf[j-2], "\n");
                                    buf[j-2][pos] = '\0';
                                    pos = strcspn (buf[j], "\n");
                                    buf[j][pos] = '\0';
                                    add_fastq_entry (h, &buf[j-3][1], &buf[j-2][0], &buf[j][0]);
                                }   /* End of one fastQ entry */
                        }       /* End of input buffer */

                    /* If we are at the end of the file */
                    if (lc < BUFFSIZE)
                        {
                            break;
                        }
                }   /* End of fastQ file */

            /* Close input stream */
            gzclose (fq);
        }   /* All files have been examined */

    return h;
}
