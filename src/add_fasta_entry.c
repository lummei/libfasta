/*! \file add_fasta_entry.c
 *  \brief Add a new entry to a fastA database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include "fasta.h"

int
add_fasta_entry (STDDB *h, char *identifier, char *sequence)
{
    int ret = 0;
    unsigned int k = 0;


    /* Enter key in hash table */
    k = hash_put (h, identifier, &ret);

    /* Check if key was entered correctly */
    if (ret == -1)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failed to enter key in "
                             "database.\n", __func__, __LINE__);
            return -1;
        }
    else
        {
            hash_value(h, k) = sequence;
            return 0;
        }
}
