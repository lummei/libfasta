/*! \file fetch_subqual.c
 *  \brief Fetch a quality substring from a fastQ database entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

char *
fetch_subqual (FQDB *h, char *identifier, unsigned long int start, unsigned long int end)
{
    unsigned int k = 0;
    size_t size = 0;
    char *substring = NULL;
    char *startptr = NULL;
    FASTQ *f;

    /* Lookup the query key from database */
    k = fqdb_get (h, identifier);

    if (k < fqdb_end(h))
        {
			f = fqdb_value(h, k);
            size = strlen (f->qual);

            if ((start < 1) || (end > size))
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: requested start and end "
                                     "positions are out of bounds\n", __func__, __LINE__);
                    return NULL;
                }
            else
                {
                    size = end - start;
                    if ((substring = malloc ((size + 1) * sizeof (char))) == NULL)
                        {
                            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                                     "memory for substring\n", __func__, __LINE__);
                            return NULL;
                        }
                    startptr = (f->qual) + (start - 1);
                    strncpy (substring, startptr, size);
                    substring[size] = '\0';
                    return substring;
                }
        }
    else
        {
            return NULL;
        }
}
