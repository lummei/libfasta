/*! \file fetch_fasta_seq.c
 *  \brief Fetch the sequence of a fastA database entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdlib.h>
#include "fasta.h"

char *
fetch_fasta_seq (STDDB *h, char *identifier)
{
    unsigned int k =  hash_get (h, identifier);

    if (k < hash_end(h))
        {
            return hash_value(h, k);
        }
    else
       {
           return NULL;
       }
}
