/*! \file fetch_qual.c
 *  \brief Fetch the quality string of a fastQ database entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include "fasta.h"

char *
fetch_qual (FQDB *h, char *identifier)
{
    unsigned int k = fqdb_get(h, identifier);
	FASTQ *f;

    if (k < fqdb_end(h))
        {
			f = fqdb_value(h, k);
            return f->qual;
        }
    else
       {
           return NULL;
       }
}
