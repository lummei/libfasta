/*! \file parse_region_string.c
 *  \brief Parses a sequence region in the form <chr>:<start>-<end>
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "fasta.h"

int
parse_region_string (char *line, struct parse_string *ps)
{
    char *p = NULL;
    char *cp = NULL;
    char *reserve = NULL;
    char seps[] = ":-";
    unsigned int chr_length = 0;

    /* Parse if string is not null */
    if (line != NULL)
        {
            /* Make a local copy of the string */
            if ((cp = strdup (line)) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                             "memory for region string\n", __func__, __LINE__);
                    return -1;
                }

            /* Parse chromosome identifier */
            p = strtok_r (cp, seps, &reserve);
            chr_length = strlen (p) + 1;

            if ((ps->chr = malloc (chr_length * sizeof (char))) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                             "memory for region string structure\n", __func__, __LINE__);
                    return -1;
                }

            strcpy (ps->chr, p);
            ps->chr[chr_length] = '\0';

            /* Parse start position */
            /* If no positions are given-- extract the full sequence */
            p = strtok_r (NULL, seps, &reserve);

            if (p == NULL)
                {
                    ps->start = 1;
                }
            else
                {
                    ps->start = strtoul (p, NULL, 0);
                }

            /* Parse end position */
            p = strtok_r (NULL, seps, &reserve);

            if (p == NULL)
                {
                    ps->end = ULONG_MAX;
                }
            else
                {
                    ps->end = strtoul (p, NULL, 0);
                }
            free (cp);
        }
    else
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: need to specify a region",
                     __func__, __LINE__);
            return -1;
        }

    return 0;
}
