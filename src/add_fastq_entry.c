/*! \file add_fastq_entry.c
 *  \brief Add a new entry to a fastQ database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
add_fastq_entry (FQDB *h, char *identifier, char *sequence, char *qual)
{
    int ret = 0;
    unsigned int k = 0;
    char *key = NULL;
	FASTQ *f = NULL;

    /* Check there are no null pointers in function arguments */
    if ((identifier == NULL) || (sequence == NULL) || (qual == NULL))
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: attempted to add "
                     "entry to database with null pointers.\n", __func__,
                     __LINE__);
            return -1;
        }
	
	/* Initialize new FASTQ structure */
	f = fastq_init ();

	/* Enter sequence and quality strings in FASTQ data structure */
	fastq_put_seq (f, sequence);
	fastq_put_qual (f, qual);
	
	/* Parse the illumina identifier line */
	parse_illumina_id (f, identifier);

    /* Enter key/identifier in the fastQ database */
	if ((key = malloc (strlen (identifier) + 1u)) == NULL)
		{
            fprintf (stderr, "[libfasta:%s:%d] Error: failed to allocate memory "
                             "for fastQ database key.\n", __func__, __LINE__);
            return -1;
		}

	strcpy (key, identifier);
    k = fqdb_put (h, key, &ret);

    /* Check if key was entered correctly */
    if (ret == -1)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failed to enter key in "
                             "fastQ database.\n", __func__, __LINE__);
            return -1;
        }
    else
        {
			/* Enter data into fastQ database */
            fqdb_value(h, k) = f;
            return 0;
        }
}

