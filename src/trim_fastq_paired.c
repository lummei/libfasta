/*! \file trim_fastq_paired.c
 *  \brief Trim the ends of paired reads in a fastQ database based on quality
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
trim_fastq_paired (FQDB *h, int trim_quality, int min_length)
{
    return 0;
}
