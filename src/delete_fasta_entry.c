/*! \file delete_fasta_entry.c
 *  \brief Remove an existing entry from the fastA database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdlib.h>
#include "fasta.h"

int
delete_fasta_entry (STDDB *h, char *identifier)
{
    unsigned int k = 0;

    k = hash_get (h, identifier);

    /* Delete the entry from the database */
    if (k < hash_end(h))
        {
            free (hash_value(h, k));
            free (hash_key(h, k));
            hash_delete (h, k);
            return 0;
        }
    else
        {
            return -1;
        }
}
