/*! \file suffix_array.c
 *  \brief Functions to construct and query a suffix array data structure
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <fasta.h>

/* Function prototypes */
static int suffix_array_compare (const void *s1, const void *s2);

SUFFIX_ARRAY *
suffix_array_create (const char *s)
{
    unsigned int i = 0;
    SUFFIX_ARRAY *sa = NULL;

    if ((sa = malloc (sizeof (SUFFIX_ARRAY))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failure to allocate "
                     "memory for suffix array\n", __func__, __LINE__);
            return NULL;
        }

    sa->n = strlen (s) + 1u;
    sa->string = s;

    if ((sa->suffix = malloc (sa->n * sizeof (char *))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failure to allocate "
                     "memory for sa->suffix\n", __func__, __LINE__);
            free (sa);
            return NULL;
        }

    /* construct array of pointers to suffixes */
    for (i = 0; i < sa->n; i++)
        {
            sa->suffix[i] = s + i;
        }

    /* this could be a lot more efficient */
    qsort (sa->suffix, sa->n, sizeof (char *), suffix_array_compare);

    return sa;
}

void
suffix_array_destroy (SUFFIX_ARRAY *sa)
{
    free (sa->suffix);
    free (sa);
}

unsigned int
suffix_array_search (SUFFIX_ARRAY *sa, const char *substring,
                     unsigned int *first)
{
    unsigned int lo = 0;
    unsigned int hi = 0;
    unsigned int mid = 0;
    unsigned int len = 0;
    int cmp = 0;

    len = strlen (substring);

    /* invariant: suffix[lo] <= substring < suffix[hi] */
    lo = 0;
    hi = sa->n;

    while ((lo + 1u) < hi)
        {
            mid = (lo + hi) / 2u;
            cmp = strncmp (sa->suffix[mid], substring, len);

            if (cmp == 0)
                {
                    /* there is a match */
                    /* search backwards and forwards for first and last */
                    for (lo = mid;
                         (lo > 0) &&
                         (strncmp (sa->suffix[lo - 1], substring, len) == 0);
                         lo--);
                    for (hi = mid;
                        (hi < sa->n) &&
                        (strncmp (sa->suffix[hi + 1], substring, len) == 0);
                        hi++);

                    if (first)
                        {
                            *first = lo;
                        }

                    return hi - lo + 1u;
                }
            else if (cmp < 0)
                {
                    lo = mid;
                }
            else
                {
                    hi = mid;
                }
        }
    return 0;
}

char *
suffix_array_BWT (SUFFIX_ARRAY *sa)
{
    unsigned int i = 0;
    char *bwt = NULL;

    if ((bwt = malloc (sa->n)) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failure to allocate "
                             "memory for bwt\n", __func__, __LINE__);
            return NULL;
        }

    for (i = 0; i < sa->n; i++)
        {
            if (sa->suffix[i] == sa->string)
                {
                    bwt[i] = '\0';
                }
            else
                {
                    bwt[i] = sa->suffix[i][-1];
                }
        }
    return bwt;
}

char *
inverse_BWT (unsigned int len, const char *s)
{
    unsigned int *successor = NULL;
    unsigned int count[UCHAR_MAX + 1u];
    unsigned int offset[UCHAR_MAX + 1u];
    unsigned int i = 0;
    unsigned int thread = 0;
    int c = 0;
    char *ret = NULL;

    if ((successor = malloc (len * sizeof (*successor))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failure to allocate "
                             "memory for successor\n", __func__, __LINE__);
            return NULL;
        }

    /* counting sort */
    for (c = 0; c <= UCHAR_MAX; c++)
        {
            count[c] = 0;
        }

    for (i = 0; i < len; i++)
        {
            count[(unsigned char) s[i]]++;
        }

    offset[0] = 0;

    for (c = 1; c <= UCHAR_MAX; c++)
        {
            offset[c] = offset[c - 1] + count[c - 1];
        }

    for (i = 0; i < len; i++)
        {
            successor[offset[(unsigned char) s[i]]++] = i;
        }

    /* find the nul */
    for (thread = 0; s[thread]; thread++);

    /* thread the result */
    if ((ret = malloc (len)) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: failure to allocate "
                             "memory for ret\n", __func__, __LINE__);
            return NULL;
        }

    for (i = 0, thread = successor[thread];
         i < len;
         i++, thread = successor[thread])
        {
            ret[i] = s[thread];
        }

    return ret;
}

static int
suffix_array_compare (const void *s1, const void *s2)
{
    return strcmp (*((const char **) s1), *((const char **) s2));
}
