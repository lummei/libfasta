/*! \file trim_fastq.c
 *  \brief Trim the ends of reads in a fastQ database based on quality
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
trim_fastq (FQDB *h, int trim_quality, int min_length)
{
    int cqual = 0;
    int max = 0;
    int max_length = 0;
    size_t i = 0;
    size_t seqlen = 0;
    unsigned int k = 0;
    FASTQ *f;

    /* Iterate through the database */
    for (k = fqdb_begin(h); k!= fqdb_end(h); k++)
        {
            if (fqdb_exists(h, k))
                {
					f = fqdb_value(h, k);
                    seqlen = strlen (f->qual);
                    max = 0;
                    max_length = seqlen;
                    cqual = 0;

                    /* Trim the ends of the read according to
                       argmax_x{\sum_{i=x+1}^l(INT-q_i)} */
                    for (i = seqlen - 1; i >= min_length; --i)
                        {
                            cqual += trim_quality - (f->qual[i] - 33);
                            if (cqual < 0)
                                {
                                    break;
                                }
                            if (cqual > max)
                                {
                                    max = cqual;
                                    max_length = i;
                                }
                        }
                    /* TODO: actually trim the sequence and quality here */
                }
        }

    return 0;
}
