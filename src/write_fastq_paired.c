/*! \file write_fastq_paired.c
 *  \brief Write an existing paired-end fastQ database to file
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include "fasta.h"

int
write_fastq_paired (FQDB *h, char *filename_base)
{
    unsigned int k1 = 0;
    unsigned int k2 = 0;
    char *filename1 = NULL;
    char *filename2 = NULL;
    gzFile outfile1;
    gzFile outfile2;

    /* Construct the names of two output files from filename_base */
    if ((filename1 = malloc (strlen (filename_base) + 10)) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                     "memory for filename1\n", __func__, __LINE__);
            return -1;
        }

    if ((filename2 = malloc (strlen (filename_base) + 10)) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                     "memory for filename2\n", __func__, __LINE__);
            return -1;
        }

    strcpy (filename1, filename_base);
    strcpy (filename2, filename_base);
    strcat (filename1, ".R1.fq.gz");
    strcat (filename2, ".R2.fq.gz");

    /* Open the first fastQ output stream */
    if ((outfile1 = gzopen (filename1, "wb")) == Z_NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the first output "
                     "fastQ file: %s.\n", __func__, __LINE__, filename1);
            return -1;
        }

    /* Open the second fastQ output stream */
    if ((outfile2 = gzopen (filename2, "wb")) == Z_NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the second output "
                     "fastQ file: %s.\n", __func__, __LINE__, filename2);
            return -1;
        }

    /* Iterate cursor through all database entries */
    for (k1 = fqdb_begin(h); k1 != fqdb_end(h); k1++)
        {
            /* Write the fastQ entry to the output file */
            if (fqdb_exists(h, k1) && (fqdb_value(h, k1)->read == 1) &&
                fqdb_value(h, k1)->has_mate)
                {
					/* Get index of mated pair */
					k2 = fqdb_value(h, k1)->mate;

					if (fqdb_exists(h, k2))
						{
							/* Write first mate to outfile stream 1 */
                            gzputc (outfile1, '@');
                            gzputs (outfile1, fqdb_key(h, k1));
							gzputc (outfile1, '\n');
							gzputs (outfile1, fqdb_value(h, k1)->seq);
							gzputs (outfile1, "\n+\n");
							gzputs (outfile1, fqdb_value(h, k1)->qual);
							gzputc (outfile1, '\n');

							/* Write second mate to outfile stream 2 */
                            gzputc (outfile2, '@');
							gzputs (outfile2, fqdb_key(h, k2));
							gzputc (outfile2, '\n');
							gzputs (outfile2, fqdb_value(h, k2)->seq);
							gzputs (outfile2, "\n+\n");
							gzputs (outfile2, fqdb_value(h, k2)->qual);
							gzputc (outfile2, '\n');
						}
                }
        }   /* End of database entries */

    /* Free alloc'ed memory */
    free (filename1);
    free (filename2);

    /* Close the two output file streams */
    gzclose (outfile1);
    gzclose (outfile2);

    return 0;
}
