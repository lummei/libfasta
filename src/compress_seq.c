/*! \file compress_seq.c
 *  \brief Compresses IUPAC alphabet into 4-bit encoding
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

char iupac_ext[17] = "ACGTRYSWKMBDHVN-\0";

COMPRESS_SEQ *
compress_seq (char *s)
{
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned short int c = 0;
    char p = 0;
    char *head = NULL;
    char *end = NULL;
    COMPRESS_SEQ *comp_str = NULL;

    /* Allocate memory for compressed sequence data structure */
    if ((comp_str = malloc (sizeof (COMPRESS_SEQ))) == NULL)
        {
            fputs ("Error: failure to allocate memory for COMPRESS_SEQ.", stderr);
            return NULL;
        }

    /* Assign length of uncompressed string to data structure */
    comp_str->ulength = strlen (s);

    /* If the uncompressed length is an odd number--
     * add one to allocated space for compressed string length */
    comp_str->clength = comp_str->ulength / 2;
    if (comp_str->ulength % 2)
        {
            comp_str->clength++;
        }

    /* Allocate memory for compressed sequence within the data structure */
    if ((comp_str->seq = malloc ((comp_str->clength + 1) * sizeof (char))) == NULL)
        {
            fputs ("Error: failure to allocate memory for comp_str->seq.", stderr);
            free (comp_str);
            return NULL;
        }

    /* Iterate through the original string and compress to two chars per byte */
    for (head = s, end = s + comp_str->ulength - 1, i = 0, j = 0; head <= end; head++, i++)
        {
            p = *head;
            c = strcspn (iupac_ext, &p);
            if (i % 2)
                {
                    comp_str->seq[j++] |= (char)(c << 4);
                }
            else
                {
                    comp_str->seq[j] = (char)(c);
                    if (head == end)
                        {
                            ++j;
                        }
                }
        }
    comp_str->seq[j] = '\0';

    return comp_str;
}
