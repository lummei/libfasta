/*! \file hash.c
 *  \brief Hashing functions for the libfasta library
 *  \version 0.8
 *  \author Heng Li with modifications by Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "fasta.h"

#define __ac_isempty(flag, i) ((flag[i>>4]>>((i&0xfU)<<1))&2)
#define __ac_isdel(flag, i) ((flag[i>>4]>>((i&0xfU)<<1))&1)
#define __ac_iseither(flag, i) ((flag[i>>4]>>((i&0xfU)<<1))&3)
#define __ac_set_isdel_false(flag, i) (flag[i>>4]&=~(1ul<<((i&0xfU)<<1)))
#define __ac_set_isempty_false(flag, i) (flag[i>>4]&=~(2ul<<((i&0xfU)<<1)))
#define __ac_set_isboth_false(flag, i) (flag[i>>4]&=~(3ul<<((i&0xfU)<<1)))
#define __ac_set_isdel_true(flag, i) (flag[i>>4]|=1ul<<((i&0xfU)<<1))
#define __ac_fsize(m) ((m) < 16? 1 : (m)>>4)
#define roundup(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
#define __hash_equal(a, b) (strcmp(a, b) == 0)
#define __fqdb_equal(a, b) (strcmp(a, b) == 0)

static const double __ac_HASH_UPPER = 0.77;

static inline unsigned int
__hash_func (const char *s)
{
    unsigned int h = (unsigned int)*s;
    if (h)
        {
            for (++s; *s; ++s)
                {
                    h = (h << 5) - h + (unsigned int)*s;
                }
        }
    return h;
}

STDDB *
hash_init (void)
{
    STDDB *h = NULL;

    h = calloc (1, sizeof (STDDB));

    return h;
}

FQDB *
fqdb_init (void)
{
    FQDB *h = NULL;

    h = calloc (1, sizeof (FQDB));

    return h;
}

int
hash_exists (STDDB *h, unsigned int x)
{
    return !__ac_iseither((h)->flags, (x));
}

int
fqdb_exists (FQDB *h, unsigned int x)
{
    return !__ac_iseither((h)->flags, (x));
}

void
hash_destroy (STDDB *h)
{
    if (h)
        {
            free (h->keys);
            free (h->flags);
            free (h->vals);
            free (h);
        }
}

void
fqdb_destroy (FQDB *h)
{
    if (h)
        {
            free (h->keys);
            free (h->flags);
            free (h->vals);
            free (h);
        }
}

void
hash_clear (STDDB *h)
{
    if (h && h->flags)
        {
            memset (h->flags, 0xaa, __ac_fsize(h->n_buckets) * sizeof (unsigned int));
            h->size = h->n_occupied = 0;
        }
}

void
fqdb_clear (FQDB *h)
{
    if (h && h->flags)
        {
            memset (h->flags, 0xaa, __ac_fsize(h->n_buckets) * sizeof (unsigned int));
            h->size = h->n_occupied = 0;
        }
}

unsigned int
hash_get (STDDB *h, char *key)
{
    if (h->n_buckets)
        {
            unsigned int i = 0;
            unsigned int k = 0;
            unsigned int last = 0;
            unsigned int mask = 0;
            unsigned int step = 0;

            mask = h->n_buckets - 1;
            k = __hash_func (key);
            i = k & mask;
            last = i;

            while ((!__ac_isempty(h->flags, i)) && (__ac_isdel(h->flags, i) || (!__hash_equal(h->keys[i], key))))
                {
                    i = (i + (++step)) & mask;

                    if (i == last)
                        {
                            return h->n_buckets;
                        }
                }
            return __ac_iseither(h->flags, i) ? h->n_buckets : i;
        }
    else
        {
            return 0;
        }
}

unsigned int
fqdb_get (FQDB *h, char *key)
{
    if (h->n_buckets)
        {
            unsigned int i = 0;
            unsigned int k = 0;
            unsigned int last = 0;
            unsigned int mask = 0;
            unsigned int step = 0;

            mask = h->n_buckets - 1;
            k = __hash_func (key);
            i = k & mask;
            last = i;

            while ((!__ac_isempty(h->flags, i)) && (__ac_isdel(h->flags, i) || (!__fqdb_equal(h->keys[i], key))))
                {
                    i = (i + (++step)) & mask;

                    if (i == last)
                        {
                            return h->n_buckets;
                        }
                }
            return __ac_iseither(h->flags, i) ? h->n_buckets : i;
        }
    else
        {
            return 0;
        }
}

/* This function uses 0.25*n_buckets bytes of working space instead of
 * [sizeof(key_t+val_t)+.25]*n_buckets.
 */

int
hash_resize (STDDB *h, unsigned int new_n_buckets)
{
    unsigned int *new_flags = NULL;
    unsigned int j = 1;
    {
        roundup(new_n_buckets);

        if (new_n_buckets < 4)
            {
                new_n_buckets = 4;
            }

        /* requested size is too small */
        if (h->size >= (unsigned int)(new_n_buckets * __ac_HASH_UPPER + 0.5))
            {
                j = 0;
            }
        /* hash table size to be changed (shrink or expand); rehash */
        else
            {
                new_flags = malloc (__ac_fsize(new_n_buckets) * sizeof (unsigned int));

                if (!new_flags)
                    {
                        return -1;
                    }

                memset (new_flags, 0xaa, __ac_fsize(new_n_buckets) * sizeof (unsigned int));

                /* expand */
                if (h->n_buckets < new_n_buckets)
                    {
                        char **new_keys = realloc (h->keys, new_n_buckets * sizeof (char *));

                        if (!new_keys)
                            {
                                free (new_flags);
                                return -1;
                            }

                        h->keys = new_keys;

                        char **new_vals = realloc (h->vals, new_n_buckets * sizeof (char *));
                        if (!new_vals)
                            {
                                free (new_flags);
                                return -1;
                            }
                        h->vals = new_vals;
                    } /* otherwise shrink */
            }
    }

    /* rehashing is needed */
    if (j)
        {
            for (j = 0; j != h->n_buckets; ++j)
                {
                    if (__ac_iseither(h->flags, j) == 0)
                        {
                            char *key = h->keys[j];
                            char *val;
                            unsigned int new_mask;

                            new_mask = new_n_buckets - 1;
                            val = h->vals[j];
                            __ac_set_isdel_true(h->flags, j);

                            /* kick-out process; sort of like in Cuckoo hashing */
                            while (1)
                                {
                                    unsigned int k = 0;
                                    unsigned int i = 0;
                                    unsigned int step = 0;

                                    k = __hash_func (key);
                                    i = k & new_mask;

                                    while (!__ac_isempty(new_flags, i))
                                        {
                                            i = (i + (++step)) & new_mask;
                                        }
                                    __ac_set_isempty_false(new_flags, i);

                                    /* kick out the existing element */
                                    if ((i < h->n_buckets) && (__ac_iseither(h->flags, i) == 0))
                                        {
                                            char *tmp = h->keys[i];
                                            h->keys[i] = key;
                                            key = tmp;
                                            tmp = h->vals[i];
                                            h->vals[i] = val;
                                            val = tmp;

                                            /* mark it as deleted in the old hash table */
                                            __ac_set_isdel_true(h->flags, i);
                                        }
                                    /* write the element and jump out of the loop */
                                    else
                                        {
                                            h->keys[i] = key;
                                            h->vals[i] = val;
                                            break;
                                        }
                                }
                        }
                }
            /* shrink the hash table */
            if (h->n_buckets > new_n_buckets)
                {
                    h->keys = realloc (h->keys, new_n_buckets * sizeof (char *));
                    h->vals = realloc (h->vals, new_n_buckets * sizeof (char *));
                }

            /* free the working space */
            free (h->flags);
            h->flags = new_flags;
            h->n_buckets = new_n_buckets;
            h->n_occupied = h->size;
            h->upper_bound = (unsigned int)(h->n_buckets * __ac_HASH_UPPER + 0.5);
        }

    return 0;
}

int
fqdb_resize (FQDB *h, unsigned int new_n_buckets)
{
    unsigned int *new_flags = NULL;
    unsigned int j = 1;
    {
        roundup(new_n_buckets);

        if (new_n_buckets < 4)
            {
                new_n_buckets = 4;
            }

        /* requested size is too small */
        if (h->size >= (unsigned int)(new_n_buckets * __ac_HASH_UPPER + 0.5))
            {
                j = 0;
            }
        /* hash table size to be changed (shrink or expand); rehash */
        else
            {
                new_flags = malloc (__ac_fsize(new_n_buckets) * sizeof (unsigned int));

                if (!new_flags)
                    {
                        return -1;
                    }

                memset (new_flags, 0xaa, __ac_fsize(new_n_buckets) * sizeof (unsigned int));

                /* expand */
                if (h->n_buckets < new_n_buckets)
                    {
                        char **new_keys = realloc (h->keys, new_n_buckets * sizeof (char *));

                        if (!new_keys)
                            {
                                free (new_flags);
                                return -1;
                            }

                        h->keys = new_keys;

                        FASTQ **new_vals = realloc (h->vals, new_n_buckets * sizeof (FASTQ *));
                        if (!new_vals)
                            {
                                free (new_flags);
                                return -1;
                            }
                        h->vals = new_vals;
                    } /* otherwise shrink */
            }
    }

    /* rehashing is needed */
    if (j)
        {
            for (j = 0; j != h->n_buckets; ++j)
                {
                    if (__ac_iseither(h->flags, j) == 0)
                        {
                            char *key = h->keys[j];
                            FASTQ *val;
                            unsigned int new_mask;

                            new_mask = new_n_buckets - 1;
                            val = h->vals[j];
                            __ac_set_isdel_true(h->flags, j);

                            /* kick-out process; sort of like in Cuckoo hashing */
                            while (1)
                                {
                                    unsigned int k = 0;
                                    unsigned int i = 0;
                                    unsigned int step = 0;

                                    k = __hash_func (key);
                                    i = k & new_mask;

                                    while (!__ac_isempty(new_flags, i))
                                        {
                                            i = (i + (++step)) & new_mask;
                                        }
                                    __ac_set_isempty_false(new_flags, i);

                                    /* kick out the existing element */
                                    if ((i < h->n_buckets) && (__ac_iseither(h->flags, i) == 0))
                                        {
                                            char *tmp = h->keys[i];
                                            h->keys[i] = key;
                                            key = tmp;
                                            FASTQ *tmpfq = h->vals[i];
                                            h->vals[i] = val;
                                            val = tmpfq;

                                            /* mark it as deleted in the old hash table */
                                            __ac_set_isdel_true(h->flags, i);
                                        }
                                    /* write the element and jump out of the loop */
                                    else
                                        {
                                            h->keys[i] = key;
                                            h->vals[i] = val;
                                            break;
                                        }
                                }
                        }
                }
            /* shrink the hash table */
            if (h->n_buckets > new_n_buckets)
                {
                    h->keys = realloc (h->keys, new_n_buckets * sizeof (char *));
                    h->vals = realloc (h->vals, new_n_buckets * sizeof (FASTQ *));
                }

            /* free the working space */
            free (h->flags);
            h->flags = new_flags;
            h->n_buckets = new_n_buckets;
            h->n_occupied = h->size;
            h->upper_bound = (unsigned int)(h->n_buckets * __ac_HASH_UPPER + 0.5);
        }

    return 0;
}

unsigned int
hash_put (STDDB *h, char *key, int *ret)
{
    unsigned int x = 0;

    /* update the hash table */
    if (h->n_occupied >= h->upper_bound)
        {
            if (h->n_buckets > (h->size << 1))
                {
                    /* clear "deleted" elements */
                    if (hash_resize (h, h->n_buckets - 1) < 0)
                        {
                            *ret = -1;
                            return h->n_buckets;
                        }
                }
            /* expand the hash table */
            else if (hash_resize (h, h->n_buckets + 1) < 0)
                {
                    *ret = -1;
                    return h->n_buckets;
                }
        } /* TODO: to implement automatically shrinking; resize() already support shrinking */
    {
        unsigned int k = 0;
        unsigned int i = 0;
        unsigned int site = 0;
        unsigned int last = 0;
        unsigned int mask = h->n_buckets - 1;
        unsigned int step = 0;

        x = site = h->n_buckets;
        k = __hash_func (key);
        i = k & mask;

        /* for speed up */
        if (__ac_isempty(h->flags, i))
            {
                x = i;
            }
        else
            {
                last = i;
                while (!__ac_isempty(h->flags, i) && (__ac_isdel(h->flags, i) || !__hash_equal(h->keys[i], key)))
                    {
                        if (__ac_isdel(h->flags, i))
                            {
                                site = i;
                            }
                        i = (i + (++step)) & mask;
                        if (i == last)
                            {
                                x = site;
                                break;
                            }
                    }
                if (x == h->n_buckets)
                    {
                        if ((__ac_isempty(h->flags, i)) && (site != h->n_buckets))
                            {
                                x = site;
                            }
                        else
                            {
                                x = i;
                            }
                    }
            }
    }
    /* not present at all */
    if (__ac_isempty(h->flags, x))
        {
            h->keys[x] = key;
            __ac_set_isboth_false(h->flags, x);
            ++h->size;
            ++h->n_occupied;
            *ret = 1;
        }
    /* deleted */
    else if (__ac_isdel(h->flags, x))
        {
            h->keys[x] = key;
            __ac_set_isboth_false(h->flags, x);
            ++h->size;
            *ret = 2;
        }
    /* Don't touch h->keys[x] if present and not deleted */
    else
        {
            *ret = 0;
        }

    return x;
}

unsigned int
fqdb_put (FQDB *h, char *key, int *ret)
{
    unsigned int x = 0;

    /* update the hash table */
    if (h->n_occupied >= h->upper_bound)
        {
            if (h->n_buckets > (h->size << 1))
                {
                    /* clear "deleted" elements */
                    if (fqdb_resize (h, h->n_buckets - 1) < 0)
                        {
                            *ret = -1;
                            return h->n_buckets;
                        }
                }
            /* expand the hash table */
            else if (fqdb_resize (h, h->n_buckets + 1) < 0)
                {
                    *ret = -1;
                    return h->n_buckets;
                }
        } /* TODO: to implement automatically shrinking; resize() already support shrinking */
    {
        unsigned int k = 0;
        unsigned int i = 0;
        unsigned int site = 0;
        unsigned int last = 0;
        unsigned int mask = h->n_buckets - 1;
        unsigned int step = 0;

        x = site = h->n_buckets;
        k = __hash_func (key);
        i = k & mask;

        /* for speed up */
        if (__ac_isempty(h->flags, i))
            {
                x = i;
            }
        else
            {
                last = i;
                while (!__ac_isempty(h->flags, i) && (__ac_isdel(h->flags, i) || !__fqdb_equal(h->keys[i], key)))
                    {
                        if (__ac_isdel(h->flags, i))
                            {
                                site = i;
                            }
                        i = (i + (++step)) & mask;
                        if (i == last)
                            {
                                x = site;
                                break;
                            }
                    }
                if (x == h->n_buckets)
                    {
                        if ((__ac_isempty(h->flags, i)) && (site != h->n_buckets))
                            {
                                x = site;
                            }
                        else
                            {
                                x = i;
                            }
                    }
            }
    }
    /* not present at all */
    if (__ac_isempty(h->flags, x))
        {
            h->keys[x] = key;
            __ac_set_isboth_false(h->flags, x);
            ++h->size;
            ++h->n_occupied;
            *ret = 1;
        }
    /* deleted */
    else if (__ac_isdel(h->flags, x))
        {
            h->keys[x] = key;
            __ac_set_isboth_false(h->flags, x);
            ++h->size;
            *ret = 2;
        }
    /* Don't touch h->keys[x] if present and not deleted */
    else
        {
            *ret = 0;
        }

    return x;
}

void
hash_delete (STDDB *h, unsigned int x)
{
    if ((x != h->n_buckets) && (!__ac_iseither(h->flags, x)))
        {
            __ac_set_isdel_true(h->flags, x);
            --h->size;
        }
}

void
fqdb_delete (FQDB *h, unsigned int x)
{
    if ((x != h->n_buckets) && (!__ac_iseither(h->flags, x)))
        {
            __ac_set_isdel_true(h->flags, x);
            --h->size;
        }
}
