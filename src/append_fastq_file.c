/*! \file append_fastq_file.c
 *  \brief Write a single fastQ database entry to existing file
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date August 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include "fasta.h"

int
append_fastq_file (FASTQ *f, char *identifier, gzFile *outfile)
{
	gzputc (*outfile, '@');
	gzputs (*outfile, identifier);
	gzputc (*outfile, '\n');
	gzputs (*outfile, f->seq);
	gzputs (*outfile, "\n+\n");
	gzputs (*outfile, f->qual);
	gzputc (*outfile, '\n');
    return 0;
}
