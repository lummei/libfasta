/*! \file delete_fastq_entry.c
 *  \brief Remove an existing entry from the fastQ database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include "fasta.h"

int
delete_fastq_entry (FQDB *h, char *identifier)
{
    unsigned int k = 0;
    FASTQ *f;

    k = fqdb_get (h, identifier);
    f = fqdb_value(h, k);

    /* Delete the entry from the database */
    if (k < fqdb_end(h) && fqdb_exists(h, k))
        {
			free (f->seq);
			free (f->qual);            
			free (f->indiv_id);
			free (f->index_sequence);
			free (f->instrument);
			free (f->flowcell_ID);
            fqdb_delete (h, k);
			free (f);
			free (fqdb_key(h, k));
            return 0;
        }
    else
        {
            return -1;
        }
}
