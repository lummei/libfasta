/*! \file fastq_put_qual.c
 *  \brief Enter a quality string in the FASTQ data structure
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
fastq_put_qual (FASTQ *f, char *qual)
{
	if ((f == NULL) || (qual == NULL))
		{
			fprintf (stderr, "[libfasta:%s:%d] Error: passed null pointer "
			                 "as argument.\n", __func__, __LINE__);
			return -1;
		}

	size_t qualen = strlen (qual);

	if ((f->qual = (char *) calloc (qualen + 1, sizeof (char))) == NULL)
		{
            fprintf (stderr, "[libfasta:%s:%d] Error: failed to "
                             "allocate memory for new quality entry.\n",
                             __func__, __LINE__);
            return -1;
		}
	
	/* Copy string to new memory location */
	strcpy (f->qual, qual);
	
	return 0;
}
