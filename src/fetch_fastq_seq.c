/*! \file fetch_fastq_seq.c
 *  \brief Fetch the sequence of a fastQ database entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

char *
fetch_fastq_seq (FQDB *h, char *identifier)
{
    unsigned int k = fqdb_get (h, identifier);
    FASTQ *f;

    if (k < fqdb_end(h) && fqdb_exists(h, k))
        {
			f = fqdb_value(h, k);
            return f->seq;
        }
    else
       {
           return NULL;
       }
}
