/*! \file write_fastq.c
 *  \brief Write an existing fastQ database to file
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <zlib.h>
#include "fasta.h"

int
write_fastq (FQDB *h, char *filename)
{
    unsigned int k = 0;
    FASTQ *f;
    gzFile outfile;

    /* Open the fastQ output stream */
    if ((outfile = gzopen (filename, "wb")) == Z_NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the output "
                     "fastQ file: %s.\n", __func__, __LINE__, filename);
            return -1;
        }

    /* Iterate cursor through all database entries */
    for (k = fqdb_begin(h); k != fqdb_end(h); k++)
        {
            /* Write the fastQ entry to the output file */
            if (fqdb_exists(h, k))
                {
					f = fqdb_value(h, k);
                    gzputc (outfile, '@');
                    gzputs (outfile, fqdb_key(h, k));
                    gzputc (outfile, '\n');
                    gzputs (outfile, f->seq);
                    gzputs (outfile, "\n+\n");
                    gzputs (outfile, f->qual);
                    gzputc (outfile, '\n');
                }
        }   /* End of database entries */

    /* Close the output file stream */
    gzclose (outfile);

    return 0;
}
