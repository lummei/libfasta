/*! \file fastq_put_seq.c
 *  \brief Enter a sequence string in the FASTQ data structure
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
fastq_put_seq (FASTQ *f, char *seq)
{
	if ((f == NULL) || (seq == NULL))
		{
			fprintf (stderr, "[libfasta:%s:%d] Error: passed null "
			                 "pointers as arguments.\n", __func__, __LINE__);
			return -1;
		}
		
	size_t seqlen = strlen (seq);

	if ((f->seq = (char *) calloc (seqlen + 1, sizeof (char))) == NULL)
		{
            fprintf (stderr, "[libfasta:%s:%d] Error: failed to "
                             "allocate memory for new sequence entry.\n",
                             __func__, __LINE__);
            return -1;
		}

	/* Copy string to new memory location */
	strcpy (f->seq, seq);
	
	return 0;
}
