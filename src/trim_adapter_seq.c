/*! \file trim_adapter_seq.c
 *  \brief Trim adapter sequence from the 5' end of fastQ database entry
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date August 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

#define FALSE 0
#define TRUE 1

int
trim_adapter_seq (FQDB *h, STDDB *adb, int orient, int edit_dist)
{
    char *subseq = NULL;
    char *copy = NULL;
	char *seqstart = NULL;
	char *qualstart = NULL;
	char *key;
    int dist = 0;
    size_t adapt_len = 0;
	size_t new_seqlen = 0;
	size_t new_qualen = 0;
	size_t bcval_length = 0;
    unsigned short int adapt_found = FALSE;
    unsigned int k = 0;
    unsigned int q = 0;
    unsigned int r = 0;
    FASTQ *f;

    /* Get length of adapter sequence */
    /* Assumes adapter sequences are all the same length */
    r = hash_begin(adb);
    adapt_len = strlen (hash_key(adb, r));

    /* Allocate memory for local substring storage */
    if ((subseq = malloc (adapt_len + 1u)) == NULL)
		{
			fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
					 "memory for substring\n", __func__, __LINE__);
			return -1;
		}

    /* Iterate through the fastQ database */
    for (k = fqdb_begin(h); k != fqdb_end(h); k++)
        {
            if (fqdb_exists(h, k) && (fqdb_value(h, k)->read == orient))
                {
					f = fqdb_value(h, k);
					key = fqdb_key(h, k);

					/* Try exact match of 5' end with adapter DB */
					strncpy (subseq, f->seq, adapt_len);
					q = hash_get(adb, subseq);
					if (q < hash_end(adb))
						{
							/* Adapter sequence found in database */
							adapt_found = TRUE;

							/* Get length of barcode value */
							bcval_length = strlen (hash_value(adb, q));

							/* Allocate memory for individual ID in FASTQ data structure */
							if ((f->indiv_id = malloc (bcval_length + 1u)) == NULL)
								{
									fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
									         "allocate memory for indivd_id string.\n",
									         __func__, __LINE__);
									return -1;
								}

							/* Copy adapter hash value to individual ID */
							strcpy (f->indiv_id, hash_value(adb, q));
						}
					else
						{
							/* Iterate through adapter DB looking for */
							/* inexact matches with given edit distance */
							for (r = hash_begin(adb); r != hash_end(adb); r++)
								{
									/* Calculate edit distance */
									if (hash_exists(adb, r))
										{
											if ((copy = malloc (adapt_len + 1u)) == NULL)
												{
													fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
															 "allocate memory for adapter copy.\n",
															 __func__, __LINE__);
													return -1;
												}
											memcpy (copy, hash_key(adb, r), adapt_len);
											dist = levenshtein (copy, subseq);
											if (dist <= edit_dist)
												{
													adapt_found = TRUE;

													/* Get length of barcode value */
													bcval_length = strlen (hash_value(adb, r));

													/* Allocate memory for individual ID in FASTQ data structure */
													if ((f->indiv_id = malloc (bcval_length + 1u)) == NULL)
														{
															fprintf (stderr, "[libfasta:%s:%d] Error: cannot "
																	 "allocate memory for indivd_id string.\n",
																	 __func__, __LINE__);
															return -1;
														}

													/* Copy adapter hash value to individual ID */
													strcpy (f->indiv_id, hash_value(adb, r));
													break;
												}
											free (copy);
										}
								}
						}
					/* Trim sequence */
					if (adapt_found)
						{
							/* Get pointers to start of non-adapter sequence */
							seqstart = (f->seq) + adapt_len;
							qualstart = (f->qual) + adapt_len;

							/* Get length of new sequences */
							new_seqlen = strlen (seqstart);
							new_qualen = strlen (qualstart);
							if (new_seqlen != new_qualen)
								{
									fprintf (stderr, "[libfasta:%s:%d] Error: trimmed sequence "
											 "and quality strings not equal length\n", __func__, __LINE__);
									return -1;
								}

							/* Reallocate memory and copy strings */
							if ((f->seq = realloc (f->seq, new_seqlen + 1u)) == NULL)
								{
									fprintf (stderr, "[libfasta:%s:%d] Error: reallocation of "
											 "fastQ sequence string failed\n", __func__, __LINE__);
									return -1;
								}
							if ((f->qual = realloc (f->qual, new_qualen + 1u)) == NULL)
								{
									fprintf (stderr, "[libfasta:%s:%d] Error: reallocation of "
											 "fastQ quality string failed\n", __func__, __LINE__);
									return -1;
								}
							strcpy (f->seq, seqstart);
							strcpy (f->qual, qualstart);

							/* Reset indicator variable */
							adapt_found = FALSE;
						}
				}
		}

	/* Deallocate local memory */
	free (subseq);

    return 0;
}
