/*! \file convert_fastq_score.c
 *  \brief Convert quality scores in a fastQ database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
convert_fastq_score (FQDB *h, int orig_score, int new_score)
{
    return 0;
}
