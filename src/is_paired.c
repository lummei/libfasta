/*! \file is_paired.c
 *  \brief Determine if database comprises paired reads
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date August 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
is_paired (FQDB *h)
{
	int is_paired = 1;
	unsigned int k = 0;
	
	for (k = fqdb_begin(h); k != fqdb_end(h); k++)
		{
			if (fqdb_exists(h, k))
				{
					if (!fqdb_value(h, k)->has_mate)
						{
							is_paired = 0;
						}
				}
		}

    return is_paired;
}
