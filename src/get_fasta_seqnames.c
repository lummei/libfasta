/*! \file get_fasta_seqnames.c
 *  \brief Get an array of fastA entry identifiers
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

char **
get_fasta_seqnames (STDDB *h)
{
    int count = 1;
    unsigned int k = 0;
    char **names = NULL;

    /* Initialize names string array */
    if ((names = malloc (sizeof (char *))) == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot allocate "
                     "memory for sequence names\n", __func__, __LINE__);
            return NULL;
        }


    /* Iterate through the database and add identifiers to names array */
    for (k = hash_begin(h); k!= hash_end(h); k++)
        {
            if (hash_exists (h, k))
                {
                    names[count - 1] = strdup (hash_key(h, k));
                    ++count;
                }


            if ((names = realloc (names, count * sizeof (char *))) == NULL)
                {
                    fprintf (stderr, "[libfasta:%s:%d] Error: cannot reallocate "
                             "memory for names array\n", __func__, __LINE__);
                    return NULL;
                }
        }

    return names;
}
