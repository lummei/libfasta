/*! \file pair_mates.c
 *  \brief Aligns all mated pairs in a fastQ database and deletes orphans
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date August 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fasta.h"

int
pair_mates (FQDB *h)
{
	unsigned int qi = 0;
	unsigned int ti = 0;
	unsigned int pos = 0;
	size_t qlen = 0;
	char *query_key = NULL;
	char *target_key = NULL;
	char seps[] = " ";
	
	if (h == NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot pass "
                     "null function argument.\n", __func__, __LINE__);
            return -1;
        }

    for (qi = fqdb_begin(h); qi != fqdb_end(h); qi++)
		{
			if (fqdb_exists(h, qi) && fqdb_value(h, qi)->read == 1)
				{
					query_key = fqdb_key(h, qi);
					qlen = strlen (query_key);
					pos = strcspn (query_key, seps);
					target_key = malloc (qlen + 1u);
					if (target_key == NULL)
						{
							fprintf (stderr, "[libfasta:%s:%d] Error: "
							         "cannot allocate memory for target "
							         "key.\n", __func__, __LINE__);
							return -1;
						}
					strncpy (target_key, query_key, pos + 1);
					target_key[pos+1] = '2';
					target_key[pos+2] = '\0';
					strcat (target_key, query_key + (pos + 2));
					ti = fqdb_get (h, target_key);
					if (fqdb_exists(h, ti))
						{
							fqdb_value(h, qi)->mate = ti;
							fqdb_value(h, qi)->has_mate = 1;
							fqdb_value(h, ti)->mate = qi;
							fqdb_value(h, ti)->has_mate = 1;
						}
				}
		}

	return 0;
}
