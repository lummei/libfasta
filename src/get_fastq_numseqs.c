/*! \file get_fastq_numseqs.c
 *  \brief Get the number of entries in fastQ database
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include "fasta.h"

int
get_fastq_numseqs (FQDB *h)
{
    return fqdb_size(h);
}
