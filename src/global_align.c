/*! \file global_align.c
 *  \brief Calculates the global sequence alignment by Needleman-Wunsch algorithm
 *  \version 0.8
 *  \author Heng Li with modifications by Daniel Garrigan
 *  \date August 2016
 *  \copyright MIT license
*/

#include <stdlib.h>
#include <stdint.h>
#include <emmintrin.h>
#include "fasta.h"

typedef struct
{
    int h;
    int e;
} eh_t;

static inline unsigned int *
push_cigar (int *n_cigar, int *m_cigar, unsigned int *cigar, int op,
            int len)
{
    if ((*n_cigar == 0) || (op != (cigar[(*n_cigar) - 1] & 0xf)))
        {
            if (*n_cigar == *m_cigar)
                {
                    *m_cigar = *m_cigar ? (*m_cigar) << 1 : 4;
                    cigar = realloc (cigar, (*m_cigar) << 2);
                }
            cigar[(*n_cigar)++] = len << 4 | op;
        }
    else
        {
            cigar[(*n_cigar) - 1] += len << 4;
        }

    return cigar;
}

int
global_align (int qlen, const unsigned char *query, int tlen, 
              const unsigned char *target, int m, const char *mat,
              int gapo, int gape, int w, int *n_cigar_,
              unsigned int **cigar_)
{
    int i = 0;
    int j = 0;
    int k = 0;
    int gapoe = gapo + gape;
    int score = 0;
    int n_col = 0;
    char *qp = NULL;
    unsigned char *z = NULL;
    eh_t *eh;

    if (n_cigar_)
        {
            *n_cigar_ = 0;
        }

    /* allocate memory */
    /* maximum #columns of the backtrack matrix */
    n_col = (qlen < (2 * w + 1)) ? qlen : 2 * w + 1;
    z = malloc (n_col * tlen);
    qp = malloc (qlen * m);
    eh = calloc (qlen + 1, 8);

    /* generate the query profile */
    for (k = i = 0; k < m; k++)
        {
            const char *p = &mat[k * m];

            for (j = 0; j < qlen; j++)
                {
                    qp[i++] = p[query[j]];
                }
        }

    /* fill the first row */
    eh[0].h = 0;
    eh[0].e = MINUS_INF;

    for (j = 1; (j <= qlen) && (j <= w); j++)
        {
            eh[j].h = -(gapo + gape * j);
            eh[j].e = MINUS_INF;
        }

    for (; j <= qlen; j++)
        {
			/* everything is -inf outside the band */
            eh[j].h = eh[j].e = MINUS_INF;
        }

    /* DP loop */
    /* target sequence is in the outer loop */
    for (i = 0; LIKELY(i < tlen); i++)
        {
            int f = MINUS_INF;
            int h1 = 0;
            int beg = 0;
            int end = 0;
            char *q = &qp[target[i] * qlen];
            unsigned char *zi = &z[i * n_col];

            beg = (i > w) ? i - w : 0;

            /* only loop through [beg,end) of the query sequence */
            end = ((i + w + 1) < qlen) ? i + w + 1 : qlen;
            h1 = (beg == 0) ? -(gapo + gape * (i + 1)) : MINUS_INF;

			/* This loop is organized in a similar way to ksw_extend()*/
			/* and ksw_sse2(), except: 1) not checking h>0 */
			/* 2) recording direction for backtracking */
            for (j = beg; LIKELY(j < end); j++)
                {
                    eh_t *p = &eh[j];
                    int h = p->h;
                    int e = p->e;
                    unsigned char d = 0;

                    p->h = h1;
                    h += q[j];
                    d = (h > e) ? 0 : 1;
                    h = (h > e) ? h : e;
                    d = (h > f) ? d : 2;
                    h = (h > f) ? h : f;
                    h1 = h;
                    h -= gapoe;
                    e -= gape;
                    d |= (e > h) ? 1 << 2 : 0;
                    e = (e > h) ? e : h;
                    p->e = e;
                    f -= gape;
                    /* if we want to halve the memory, use one bit only, instead of two */
                    d |= (f > h) ? 2 << 4 : 0;
                    f = (f > h) ? f : h;
                    /* z[i,j] keeps h for the current cell and e/f for the next cell */
                    zi[j - beg] = d;
                }

            eh[end].h = h1;
            eh[end].e = MINUS_INF;
        }

    score = eh[qlen].h;

    /* Backtrack */
    if (n_cigar_ && cigar_)
        {
            int n_cigar = 0;
            int m_cigar = 0;
            int which = 0;
            unsigned int *cigar = 0;
            unsigned int tmp = 0;

            i = tlen - 1;

            /* (i,k) points to the last cell */
            k = (i + w + 1 < qlen ? i + w + 1 : qlen) - 1;

            while ((i >= 0) && (k >= 0))
                {
                    which = z[i * n_col + (k - (i > w ? i - w : 0))] >> (which << 1) & 3;

                    if (which == 0)
                        {
                            cigar = push_cigar (&n_cigar, &m_cigar, cigar, 0, 1);
                            --i;
                            --k;
                        }
                    else if (which == 1)
                        {
                            cigar = push_cigar (&n_cigar, &m_cigar, cigar, 2, 1);
                            --i;
                        }
                    else
                        {
                            cigar = push_cigar (&n_cigar, &m_cigar, cigar, 1, 1);
                            --k;
                        }
                }

            if (i >= 0)
                {
                    cigar = push_cigar (&n_cigar, &m_cigar, cigar, 2, i + 1);
                }

            if (k >= 0)
                {
                    cigar = push_cigar (&n_cigar, &m_cigar, cigar, 1, k + 1);
                }

            /* Reverse CIGAR */
            for (i = 0; i < n_cigar >> 1; i++)
                {
                    tmp = cigar[i];
                    cigar[i] = cigar[n_cigar - 1 - i];
                    cigar[n_cigar - 1 - i] = tmp;
                }

            *n_cigar_ = n_cigar;
            *cigar_ = cigar;
        }

    free (eh);
    free (qp);
    free (z);

    return score;
}
