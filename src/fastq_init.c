/*! \file fastq_init.c
 *  \brief Initialize a FASTQ data structure
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include "fasta.h"

FASTQ *
fastq_init (void)
{
	FASTQ *f = NULL;

	f = (FASTQ *) calloc (1, sizeof(FASTQ));

	if (f == NULL)
		{
            fprintf (stderr, "[libfasta:%s:%d] Error: failed to "
                             "allocate memory for new FASTQ structure.\n",
                             __func__, __LINE__);
		}
	f->has_mate = 0;

	return f;
}
	
