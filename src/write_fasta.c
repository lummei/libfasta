/*! \file write_fasta.c
 *  \brief Write an existing fastA database to file
 *  \version 0.8
 *  \author Daniel Garrigan
 *  \date July 2016
 *  \copyright MIT license
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include "fasta.h"

/*! \def LINE_LENGTH
 *  \brief The length of sequence line in output fastA file
 */

#define LINE_LENGTH 80

int
write_fasta (STDDB *h, char *filename)
{
    size_t i = 0;
    unsigned int k = 0;
    gzFile outfile;

    /* Open the fastA output stream */
    if ((outfile = gzopen (filename, "wb")) == Z_NULL)
        {
            fprintf (stderr, "[libfasta:%s:%d] Error: cannot open the output "
                     "fastA file: %s.\n", __func__, __LINE__, filename);
            return -1;
        }

    /* Iterate cursor through all database entries */
    for (k = hash_begin(h); k != hash_end(h); k++)
        {
            /* Write the fastA entry identifier to the output file */
            gzputc (outfile, '>');
            gzputs (outfile, hash_key(h, k));

            /* Write the fastA entry sequence to the output file */
            for (i = 0; i < strlen (hash_value(h, k)); i++)
                {
                    if (i % LINE_LENGTH)
                        {
                            gzputc (outfile, (hash_value(h,k))[i]);
                        }
                    else
                        {
                            gzputc (outfile, '\n');
                            gzputc (outfile, (hash_value(h, k))[i]);
                        }
                } /* End of sequence entry */
            gzputc (outfile, '\n');
        }   /* End of database entries */

    /* Close the output file stream */
    gzclose (outfile);

    return 0;
}
